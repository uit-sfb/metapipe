addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.6.0")
addSbtPlugin("com.eed3si9n" % "sbt-buildinfo" % "0.10.0")
addSbtPlugin("com.gilcloud" % "sbt-gitlab" % "0.0.6")
addSbtPlugin("com.typesafe.sbt" % "sbt-git" % "1.0.1")
