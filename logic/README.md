# Logics

This folder gathers the custom logic implementations.
Since Meta-pipe can only work with Docker images, each logic should be dockerized.
