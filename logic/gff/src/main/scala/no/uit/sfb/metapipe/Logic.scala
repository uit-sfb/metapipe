package no.uit.sfb.metapipe

import java.io.{FileWriter, PrintWriter}
import java.nio.file.Path

import no.uit.sfb.metapipe.format._
import no.uit.sfb.scalautils.common.FileUtils

object Logic {
  protected def writeToFile(features: Iterator[Gff3Data],
                            filepath: Path): Unit = {
    val out = new PrintWriter(new FileWriter(filepath.toString))
    try {
      out.println("##gff-version 3")
      features foreach { f =>
        out.println(f.asString)
      }
      /*out.println("##FASTA")
      IOUtils.copyLarge(fastaSource, out)*/
    } finally {
      out.close()
    }
  }

  def exec(config: Config): Unit = {
    val formats: Seq[(FormatLike[_], Path)] = Seq(config.priam.map {
      PriamData -> _
    }, config.diamond.map { DiamondData -> _ }, config.interpro.map {
      InterproData -> _
    }).flatten
    lazy val features = formats.foldLeft(MgaData.toGff(config.mgaOut)) {
      case (acc, (format, path)) =>
        format.addToGff(acc, path)
    }

    FileUtils.createDirs(config.outDir)
    writeToFile(features, config.outDir.resolve("features.gff"))
  }
}
