package no.uit.sfb.metapipe

import java.nio.file.Path

case class Config(outDir: Path = null,
                  mgaOut: Path = null,
                  priam: Seq[Path] = Seq(),
                  diamond: Seq[Path] = Seq(),
                  interpro: Seq[Path] = Seq())
