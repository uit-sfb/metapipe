package no.uit.sfb.metapipe.format

import java.nio.file.Path

import scala.io.Source

trait FormatDataLike {
  def geneId: String
  def toGff: Gff3Data
}

trait FormatLike[A <: FormatDataLike] {
  def apply(p: Path): (Iterator[A], Source)
  def addToGff(data: Iterator[Gff3Data], filepath: Path): Iterator[Gff3Data] = {
    if (filepath == null)
      data
    else {
      val (it, src) = apply(filepath)
      val part = try {
        it.toVector //we need to materialize the iterator otherwise 'find' would consume it
      } finally {
        src.close()
      }
      data map { gene =>
        part.find(_.geneId == gene.seqid) match {
          case Some(d) =>
            gene.merge(d.toGff)
          case _ => gene
        }
      }
    }
  }
}
