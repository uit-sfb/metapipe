package no.uit.sfb.metapipe.format

import java.nio.file.Path

import scala.io.Source
import scala.util.matching.Regex

case class DiamondData(dbName: String,
                       geneId: String,
                       sseqId: String,
                       geneDescription: String)
    extends FormatDataLike {
  lazy val toGff = {
    val reg = "^\\S+ (.*?\\[.+?\\])".r
    val oDescr = reg.findFirstMatchIn(geneDescription).map { m =>
      m.group(m.groupCount)
    }
    Gff3Data(
      seqid = geneId,
      featureAttributes =
        (s"${dbName}_accession", sseqId) +: oDescr.toSeq.map { descr =>
          (s"${dbName}_description", descr)
        }
    )
  }
}

object DiamondData extends FormatLike[DiamondData] {
  protected def fromList(args: List[String],
                         dbName: String): Option[DiamondData] = args match {
    case id :: sseqId :: description :: _ :: _ :: _ :: _ :: _ :: _ :: _ :: _ :: _ :: _ =>
      Some(DiamondData(dbName, id, sseqId, description))
    case _ => None
  }

  def apply(p: Path): (Iterator[DiamondData], Source) = {
    val reg = "^diamond-(\\S+)\\.out$".r
    val fileName = p.getFileName.toString
    val dbName = fileName match {
      case reg(dbName) =>
        dbName.capitalize
      case _ =>
        "Diamond"
    }
    println(s"DbName: '$dbName' (from file: '$fileName')")
    val source = Source.fromFile(p.toString)
    def processFileDiamond(): Iterator[String] = {
      val res = source
        .getLines()
        .foldLeft[(Iterator[String], String)]((Iterator[String](), null)) {
          case ((acc, last), i)
              if last != null && (i
                .split("\\t")
                .head == last.split("\\t").head) =>
            (acc, i)
          case ((acc, _), i) => (acc ++ Iterator(i), i)
        }
      res._1
    }
    val src = processFileDiamond()
    val it = src.flatMap { l =>
      val p = l.trim.replaceAll(" +", " ").split("\\t").toList
      DiamondData.fromList(p, dbName)
    }
    (it, source)
  }
}
