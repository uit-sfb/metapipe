package no.uit.sfb.metapipe

import java.io.File
import no.uit.sfb.info.gff.BuildInfo
import scopt.OParser

import java.nio.file.Paths

object Main extends App {
  try {
    val builder = OParser.builder[Config]

    val name = BuildInfo.name
    val ver = BuildInfo.version

    val parser = {
      import builder._
      OParser.sequence(
        programName(name),
        head(name, ver),
        opt[File]("outdir").required
          .action((x, c) => c.copy(outDir = x.toPath))
          .text("Output directory."),
        opt[File]("mga").required
          .action((x, c) => c.copy(mgaOut = x.toPath))
          .text("Path to mga output file."),
        opt[String]("priam")
          .action((x, c) => c.copy(priam = x.split(',').map { Paths.get(_) }))
          .text("Comma separated list of Priam output files."),
        opt[String]("diamond")
          .action((x, c) => c.copy(diamond = x.split(',').map { Paths.get(_) }))
          .text("Comma separated list of Diamond output files."),
        opt[String]("interpro")
          .action(
            (x, c) => c.copy(interpro = x.split(',').map { Paths.get(_) })
          )
          .text("Comma separated list of Interproscan output files.")
      )
    }

    val check = {
      import builder._
      OParser.sequence(checkConfig(c => success))
    }

    OParser.parse(parser ++ check, args, Config()) match {
      case Some(config) =>
        Logic.exec(config)
      case _ =>
        // arguments are bad, error message will have been displayed
        throw new IllegalArgumentException()
    }
  } catch {
    case e: Throwable =>
      println(e.toString)
      println(e.printStackTrace())
      sys.exit(1)
  }
}
