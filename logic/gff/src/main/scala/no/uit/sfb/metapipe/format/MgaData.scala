package no.uit.sfb.metapipe.format

import java.nio.file.Path

import scala.collection.mutable
import scala.io.Source

case class MgaData(geneId: String,
                   start: String,
                   end: String,
                   strand: String,
                   phase: String)
    extends FormatDataLike {
  lazy val toGff = Gff3Data(
    seqid = geneId,
    featureType = "CDS",
    start = start,
    end = end,
    strand = strand,
    phase = phase,
    featureAttributes = Seq(("ID", geneId), ("Name", geneId))
  )
}

object MgaData extends FormatLike[MgaData] {
  protected def fromList(args: List[String]): Option[MgaData] =
    args match {
      case geneId :: start :: stop :: strand :: phase :: _ :: Nil =>
        Some(MgaData(geneId, start, stop, strand, phase))
      case _ => None
    }

  def apply(p: Path): (Iterator[MgaData], Source) = {
    val source = Source.fromFile(p.toString)
    def processFileMga(): Iterator[List[String]] = {
      val res = source
        .getLines()
        .foldLeft[(Iterator[mutable.Buffer[String]], mutable.Buffer[String])](
          (Iterator[mutable.Buffer[String]](), null)
        ) {
          case ((acc, _), s)
              if s.startsWith("#") && s
                .contains("multi") => //We use multi just to know if it is the first line of a header or not
            val latest = mutable.Buffer(s.split(" ")(1))
            (acc ++ Iterator(latest), latest)
          case ((acc, last), s) if s.startsWith("#") => (acc, last)
          case ((acc, last), s) =>
            last += (last.head + "_" + s)
            (acc, last)
        }
      res._1 map { _.toList }
    }
    val it = processFileMga().flatMap { l =>
      val data = l.tail.flatMap { s => //We take the tail since the first element is the id
        val mgaGene = s.trim.replaceAll(" +", " ").split("\\t", 6).toList
        MgaData.fromList(mgaGene)
      }
      data
    }
    (it, source)
  }

  def toGff(filepath: Path): Iterator[Gff3Data] = {
    if (filepath == null)
      Iterator()
    else {
      val (mgaPart, _) = MgaData(filepath)
      mgaPart map { _.toGff }
    }
  }
}
