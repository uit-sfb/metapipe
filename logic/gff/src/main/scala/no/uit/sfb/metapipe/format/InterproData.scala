package no.uit.sfb.metapipe.format

import java.nio.file.Path

import scala.io.Source

case class InterproData(proteinAccession: String,
                        analysisDb: String,
                        signatureAccession: String,
                        signatureDescription: String,
                        score: String,
                        status: String = "T",
                        sAccession: String = "",
                        sDescription: String = "",
                        goAnnotation: String = "",
                        pathwaysAnnotation: String = "")
    extends FormatDataLike {
  val geneId = proteinAccession
  lazy val toGff = {
    val attrs =
      Seq(
        Some((s"${analysisDb}_accession", signatureAccession)),
        if (signatureDescription.nonEmpty)
          Some((s"${analysisDb}_description", signatureDescription))
        else None,
        if (goAnnotation.nonEmpty) Some(("Ontology_term", goAnnotation))
        else None
      )
    Gff3Data(seqid = proteinAccession, featureAttributes = attrs.flatten)
  }
}

object InterproData extends FormatLike[InterproData] {
  protected def fromList(args: List[String]): Option[InterproData] =
    args match {
      case h :: _ :: _ :: analysis :: access :: desc :: _ :: _ :: score :: _ :: _ :: Nil =>
        Some(InterproData(h, analysis, access, desc, score))
      case h :: _ :: _ :: analysis :: access :: desc :: _ :: _ :: score :: _ :: _ :: saccess :: Nil =>
        Some(
          InterproData(h, analysis, access, desc, score, sAccession = saccess)
        )
      case h :: _ :: _ :: analysis :: access :: desc :: _ :: _ :: score :: _ :: _ :: saccess :: sdesc :: Nil =>
        Some(
          InterproData(
            h,
            analysis,
            access,
            desc,
            score,
            sAccession = saccess,
            sDescription = sdesc
          )
        )
      case h :: _ :: _ :: analysis :: access :: desc :: _ :: _ :: score :: _ :: _ :: saccess :: sdesc :: goann :: Nil =>
        Some(
          InterproData(
            h,
            analysis,
            access,
            desc,
            score,
            sAccession = saccess,
            sDescription = sdesc,
            goAnnotation = goann
          )
        )
      case h :: _ :: _ :: analysis :: access :: desc :: _ :: _ :: score :: _ :: _ :: saccess :: sdesc :: goann :: pathwayann :: Nil =>
        Some(
          InterproData(
            h,
            analysis,
            access,
            desc,
            score,
            sAccession = saccess,
            sDescription = sdesc,
            goAnnotation = goann,
            pathwaysAnnotation = pathwayann
          )
        )
      case _ => None
    }

  def apply(p: Path): (Iterator[InterproData], Source) = {
    val source = Source.fromFile(p.toString)
    val it = source.getLines() flatMap { l =>
      InterproData.fromList(l.split("\\t").toList)
    }
    (it, source)
  }

  override def addToGff(data: Iterator[Gff3Data],
                        filepath: Path): Iterator[Gff3Data] = {
    if (filepath == null)
      data
    else {
      val (it, src) = InterproData(filepath)
      val interproPart = try {
        it.toVector //we need to materialize the iterator otherwise 'collect' would consume it
      } finally {
        src.close()
      }
      data map { gene =>
        val (coilData, interproOut) = interproPart
          .collect { //Assuming that they are always following each other, we could do better than that
            case e: InterproData if e.proteinAccession == gene.seqid =>
              e.toGff
          }
          .partition(
            _.featureAttributes.toMap.keySet.contains("Coils_accession")
          )

        val sortedData = interproOut
          .flatMap(_.featureAttributes)
          .groupBy(_._1)
          .flatMap {
            case (k, v) =>
              val matches = v
                .map {
                  _._2
                }
                .distinct
                .zipWithIndex
              matches.map {
                case (s, idx) =>
                  val idxStr = if (idx > 0) s"_$idx" else ""
                  (k + idxStr, s)
              }
          }

        val coilCount =
          if (coilData.nonEmpty) Seq(("Coils", coilData.size.toString))
          else Seq()

        gene.merge(Gff3Data(featureAttributes = sortedData.toSeq ++ coilCount))
      }
    }
  }
}
