package no.uit.sfb.metapipe.format

import java.nio.file.Path

import scala.io.Source

case class PriamData(matchId: String, geneId: String, score: String)
    extends FormatDataLike {
  lazy val toGff = Gff3Data(
    seqid = geneId,
    score = score,
    featureAttributes = Seq(("PRIAM", matchId))
  )
}

object PriamData extends FormatLike[PriamData] {
  protected def fromList(args: List[String]): Option[PriamData] = args match {
    case matchId :: geneId :: _ :: score :: Nil =>
      Some(PriamData(matchId, geneId, score))
    case _ => None
  }

  def apply(p: Path): (Iterator[PriamData], Source) = {
    val source = Source.fromFile(p.toString)
    def processFilePriam(): Iterator[String] = {
      val iterator = source.getLines()
      val res = iterator.takeWhile(!_.contains("#Unpredicted"))
      res.filter(l => !(l.isEmpty || l.contains("#")))
    }
    val priamOut = processFilePriam()
    val it = priamOut.flatMap { l =>
      val fields = l.trim.replaceAll(" +", " ").split("\\t", 4).toList
      PriamData.fromList(fields)
    }
    (it, source)
  }
}
