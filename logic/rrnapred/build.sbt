import com.typesafe.sbt.packager.docker.{Cmd, DockerPermissionStrategy}

name := "rrnapred"
scalaVersion := "2.13.6"
organization := "no.uit.sfb"
organizationName := "SfB"

lazy val gitCommit = settingKey[String]("Git SHA")
lazy val gitRefName = settingKey[String]("Git branch or tag")
//lazy val isRelease = settingKey[Boolean]("Is release")
lazy val dockerRegistry = settingKey[String]("Docker registry")
lazy val dockerImageName = settingKey[String]("Docker image name")

useJGit

ThisBuild / gitCommit := { sys.env.getOrElse("CI_COMMIT_SHA", "unknown") }
ThisBuild / gitRefName := {
  sys.env.getOrElse("CI_COMMIT_REF_NAME", git.gitCurrentBranch.value)
}
//ThisBuild / isRelease := { sys.env.getOrElse("CI_COMMIT_REF_PROTECTED", "false").toBoolean && sys.env.getOrElse("CI_COMMIT_TAG", "").nonEmpty}
ThisBuild / dockerRegistry := {
  sys.env.getOrElse("CI_REGISTRY", "registry.gitlab.com")
}
ThisBuild / dockerImageName := {
  s"${sys.env.getOrElse("CI_REGISTRY_IMAGE", s"${dockerRegistry.value}/${organizationName.value.toLowerCase}/metapipe")}"
}
ThisBuild / version := gitRefName.value //Do not use slug here as it would replace '.' with '-'

ThisBuild / resolvers += "gitlab" at "https://gitlab.com/api/v4/groups/2067095/-/packages/maven"

lazy val scalaUtilsVersion = "0.3.3"

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.2.11" % Test,
  "com.github.scopt" %% "scopt" % "4.0.1",
  "no.uit.sfb" %% "scala-utils-genomiclib" % scalaUtilsVersion,
  "no.uit.sfb" %% "scala-utils-common" % scalaUtilsVersion,
  "ch.qos.logback" % "logback-classic" % "1.2.11"
)

enablePlugins(JavaAppPackaging, DockerPlugin)

val genomicToolRegistry = "registry.gitlab.com/uit-sfb/genomic-tools"

ThisBuild / gitCommit := { sys.env.getOrElse("CI_COMMIT_SHA", "unknown") }
ThisBuild / gitRefName := {
  sys.env.getOrElse("CI_COMMIT_REF_NAME", git.gitCurrentBranch.value)
}
ThisBuild / version := gitRefName.value //Do not use slug here as it would replace '.' with '-'
dockerLabels := Map(
  "gitCommit" -> s"${gitCommit.value}@${git.gitCurrentBranch.value}"
)
Docker / dockerRepository := Some(dockerRegistry.value)
Docker / dockerUsername := Some(
  dockerImageName.value.split("/").tail.mkString("/")
)
dockerAlias := {
  dockerAlias.value.copy(tag = Some(s"${version.value}"))
}
dockerBaseImage := "openjdk:11.0.13-jre-slim"
Docker / daemonUser := "sfb"
dockerCommands := dockerCommands.value.flatMap {
  case cmd @ Cmd("USER", args @ _*) if args.contains("1001:0") =>
    Seq(
      Cmd(
        "RUN",
        "apt update && apt install -y procps && rm -rf /var/lib/apt/lists/*"
      ),
      cmd,
      Cmd("COPY", s"--from=$genomicToolRegistry/hmmer:3.1b2 /app/hmmer /app"),
      Cmd("COPY", s"--from=$genomicToolRegistry/hmm16s:1 /db/hmm16s /data")
    )
  case cmd => Seq(cmd)
}

//dockerPermissionStrategy := DockerPermissionStrategy.None

enablePlugins(BuildInfoPlugin)
buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion)
buildInfoPackage := s"${organization.value}.info.${name.value.replace('-', '_')}"
