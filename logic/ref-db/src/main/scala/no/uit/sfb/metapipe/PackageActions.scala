package no.uit.sfb.metapipe

import java.net.URL
import java.nio.file.{Path, Paths}

import no.uit.sfb.scalautils.common.FileUtils
import no.uit.sfb.scalautils.json.Json

import scala.sys.process.Process
import scala.util.Try
import scala.sys.process._

object PackageActions {
  def apply(c: PackagesConfig): Unit = {
    lazy val depsTree: Map[String, Package] =
      PackageLister.listAll(c.artifactsUrl)

    c.cmd match {
      case "list" =>
        val artifacts = depsTree.values.toSeq
          .sortBy(_.packageName)
          .flatMap(
            _.artifacts.values.toSeq
              .sorted(Artifact)
              .map(_.artifactName)
          )
        println(artifacts.mkString("\n"))
      case "latest" =>
        depsTree.get(c.packageName) match {
          case Some(up) =>
            val art = up.get(None)
            println(art.version)
          case None =>
            throw new Exception(s"Could not find package '${c.packageName}'")
        }
      case "download" =>
        val items: Map[Package, Option[String]] =
          (c.packages map { nameAndVersion =>
            val split = nameAndVersion.split('=')
            val packageName = split.head
            val oVersion = split.tail.headOption
            depsTree.get(packageName) match {
              case Some(up) =>
                up -> (oVersion map { ver =>
                  ver.split('_').toList match {
                    case a :: Nil => a
                    case _ =>
                      throw new Exception(
                        s"Wrong format for version '$ver'. Expected <depVersion>"
                      )
                  }
                })
              case None =>
                throw new IllegalArgumentException(
                  s"Package $packageName not defined."
                )
            }
          }).toMap
        download(items, c.dir, c.artifactsUrl, c.overwrite, c.force, c.cleanup)
      case "download-all" =>
        val items: Map[Package, Option[String]] =
          depsTree map {
            case (_, packager) =>
              packager -> None
          }
        download(items, c.dir, c.artifactsUrl, c.overwrite, c.force, c.cleanup)
      case "create" =>
        create(
          c.tmpDir.resolve("metakube"),
          c.imageName,
          c.dataDir,
          depsTree,
          c.overwrite
        )
    }
  }

  private def create(tmpDir: Path,
                     imageName: String,
                     dataDir: Option[String],
                     depsTree: Map[String, Package],
                     overwrite: Boolean): Unit = {
    val dataPath = Paths.get(dataDir.getOrElse(throw new IllegalArgumentException("Please provide -r argument")))

    println(s"Packaging $imageName...")
    val (_, toolName, tag) = {
      val (iName, iTag) = imageName.split(':').toList match {
        case n :: t :: Nil => n -> t
        case n :: Nil      => n -> "latest"
        case _             => "" -> ""
      }
      val split = iName.split('/')
      (split.init.mkString("'"), split.last, iTag)
    }
    val thisArtifact = {
      val splt = tag.split('_')
      Artifact(toolName, splt.headOption.getOrElse(""))
    }
    val oPrevious
      : Option[Artifact] = depsTree.get(thisArtifact.packageName) flatMap {
      up =>
        Try {
          up.get(Some(thisArtifact.version))
        }.toOption
    }
    if (oPrevious.nonEmpty && !overwrite)
      println(
        s"Found artifact. Packaging cancelled."
      )
    else {
      val copyList: Seq[Path] = Seq(Paths.get("/app"), Paths.get("/db"))
      val cpToPath = tmpDir.resolve(toolName).resolve(tag)
      FileUtils.deleteDirIfExists(cpToPath)
      FileUtils.createDirs(cpToPath)
      val containerId = s"docker create $imageName none".!!.trim()
      copyList foreach { src =>
        s"docker cp $containerId:$src $cpToPath".! //may fail if src does not exist
      }
      s"docker rm $containerId".!!

      println("Compressing...")
      val artifName = thisArtifact.artifactName
      val tmpArtifactPath =
        tmpDir.resolve(artifName)
      FileUtils.deleteFileIfExists(tmpArtifactPath)
      Process(
        s"""tar -zcf ${tmpDir.relativize(tmpArtifactPath)} --remove-files ${tmpDir
          .relativize(cpToPath)}""",
        tmpDir.toFile
      ).!!
      //Cannot do tar and move in one step because of the relative paths.
      FileUtils.moveFileToDir(tmpArtifactPath, dataPath.resolve(toolName))
    }
  }

  private def download(items: Map[Package, Option[String]],
                       dir: Path,
                       artifactsUrl: URL,
                       overwrite: Boolean,
                       force: Boolean,
                       cleanup: Boolean): Unit = {
    if (items.isEmpty)
      println(s"Empty package list. Nothing to download.")
    items foreach {
      case (up, oDepVersion) =>
        val artifact = up.get(oDepVersion)
        if (cleanup) {
          artifact.removeOlderVersions(dir)
        }
        artifact.download(dir, artifactsUrl, overwrite, force)
    }
  }
}
