import com.typesafe.sbt.packager.docker.Cmd

name := "ref-db"
scalaVersion := "2.13.6"
organization := "no.uit.sfb"
organizationName := "SfB"

maintainer := "mmp@uit.no"

lazy val gitCommit = settingKey[String]("Git SHA")
lazy val gitRefName = settingKey[String]("Git branch or tag")
lazy val dockerRegistry = settingKey[String]("Docker registry")
lazy val dockerImageName = settingKey[String]("Docker image name")
ThisBuild / gitCommit := { sys.env.getOrElse("CI_COMMIT_SHA", "unknown") }
ThisBuild / gitRefName := {
  sys.env.getOrElse("CI_COMMIT_REF_NAME", git.gitCurrentBranch.value)
}
ThisBuild / dockerRegistry := {
  sys.env.getOrElse("CI_REGISTRY", "registry.gitlab.com")
}
ThisBuild / dockerImageName := {
  s"${sys.env.getOrElse("CI_REGISTRY_IMAGE", s"${dockerRegistry.value}/${organizationName.value.toLowerCase}/metapipe")}"
}
ThisBuild / version := gitRefName.value //Do not use slug here as it would replace '.' with '-'

ThisBuild / resolvers += "gitlab" at "https://gitlab.com/api/v4/groups/2067095/-/packages/maven"

useJGit

enablePlugins(JavaAppPackaging, DockerPlugin)
publishConfiguration := publishConfiguration.value.withOverwrite(true)

dockerLabels := Map(
  "gitCommit" -> s"${gitCommit.value}@${git.gitCurrentBranch.value}"
)
Docker / dockerRepository := Some("registry.gitlab.com")
Docker / dockerUsername := Some("uit-sfb/metapipe")
//dockerChmodType := DockerChmodType.Custom("u=rX,g=rX,o=rX")
Docker / daemonUser := "sfb" //"in Docker" needed for this parameter

lazy val scalaUtilsVersion = "0.3.3"

libraryDependencies ++= Seq(
  "com.github.scopt" %% "scopt" % "4.0.1",
  "no.uit.sfb" %% "scala-utils-common" % scalaUtilsVersion,
  "no.uit.sfb" %% "scala-utils-json" % scalaUtilsVersion,
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.4",
  "ch.qos.logback" % "logback-classic" % "1.2.11"
)

enablePlugins(BuildInfoPlugin)
buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion)
buildInfoPackage := s"${organization.value}.info.${name.value.replace('-', '_')}"
