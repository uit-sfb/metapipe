package no.uit.sfb.metapipe.mapseq2krona

import com.typesafe.scalalogging.LazyLogging

import java.io.PrintWriter
import scala.io.Source

object Logic extends LazyLogging {
  //Attention: This algorithm only works if the inputs file is sorted by increasing taxonomy level
  def exec(config: Config): Unit = {
    val src = Source.fromFile(config.inputPath.toFile)
    try {
      val it = src.getLines()
      it.drop(3) //Remove header
      val kvPairs: Iterator[(Seq[String], Int)] = it.map { l =>
        val splt = l.split('\t')
        splt(2).split(';').toSeq -> splt(3).toInt
      }
      val rawUpdated = kvPairs.foldLeft(List[(Seq[String], Int)]()) {
        case (acc, v) =>
          val updatedParents =
            if (v._1.size > 1)
              acc.map {
                case (k, value) if k == v._1.init => k -> (value - v._2)
                case c                            => c
              } else
              acc
          updatedParents :+ v
      }
      val cleanUpdated = rawUpdated.collect {
        case (k, v) if v > 0  => Some(k -> v)
        case (_, v) if v == 0 => None
        case (k, v) if v < 0 =>
          throw new Exception(s"Negative count for '${k.mkString(";")}'")
      }.flatten
      val printer = new PrintWriter(config.outPath.toFile)
      try {
        cleanUpdated.foreach {
          case (k, v) =>
            val line =
              (v.toString +: "root" +: "cellular organisms" +: k).mkString("\t")
            printer.println(line)
        }
      } finally printer.close()
    } finally src.close()
  }
}
