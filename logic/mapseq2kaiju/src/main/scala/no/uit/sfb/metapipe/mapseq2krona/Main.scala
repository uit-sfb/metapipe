package no.uit.sfb.metapipe.mapseq2krona

import java.io.File

import no.uit.sfb.info.mapseq2kaiju.BuildInfo
import scopt.OParser

object Main extends App {
  try {
    val builder = OParser.builder[Config]

    val name = BuildInfo.name
    val ver = BuildInfo.version

    val parser = {
      import builder._
      OParser.sequence(
        programName(name),
        head(name, ver),
        opt[File]('i', "input")
          .action((x, c) => c.copy(inputPath = x.toPath))
          .required()
          .text("Path to Mapseq outcounts file"),
        opt[File]('o', "oout")
          .action((x, c) => c.copy(outPath = x.toPath))
          .required()
          .text("Path to output file"),
        help('h', "help")
          .text("Prints this usage text"),
        version('v', "version")
      )
    }

    val check = {
      import builder._
      OParser.sequence(checkConfig(c => success))
    }

    OParser.parse(parser ++ check, args, Config()) match {
      case Some(config) =>
        Logic.exec(config)
      case _ =>
        // arguments are bad, error message will have been displayed
        throw new IllegalArgumentException()
    }
  } catch {
    case e: Throwable =>
      println(e.toString)
      println(e.printStackTrace())
      sys.exit(1)
  }
}
