package no.uit.sfb.metapipe.mapseq2krona

import java.nio.file.Path

case class Config(inputPath: Path = null, outPath: Path = null)
