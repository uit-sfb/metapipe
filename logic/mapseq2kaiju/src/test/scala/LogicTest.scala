import no.uit.sfb.metapipe.mapseq2krona.{Config, Logic}
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

import java.nio.file.{Files, Paths}
import scala.io.Source

class DbTest extends AnyFunSpec with Matchers {
  private val inputPath = Paths.get(getClass.getResource("/test.tsv").getPath)
  private val fasitPath = Paths.get(getClass.getResource("/fasit.tsv").getPath)
  describe("Logic") {
    it(s"should properly convert input into fasit") {
      val tmpPath = Files.createTempFile("test", ".tsv")
      val conf = Config(inputPath, tmpPath)
      Logic.exec(conf)
      Source.fromFile(tmpPath.toFile).mkString.replace("\r", "") should be(
        Source.fromFile(fasitPath.toFile).mkString.replace("\r", "")
      )
      Files.deleteIfExists(tmpPath)
    }
  }
}
