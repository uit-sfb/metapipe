package no.uit.sfb.nqs.frontend.custom

import no.uit.sfb.nqs.frontend.widgets.NqsWidget

import scala.scalajs.js.annotation.JSExportTopLevel

@JSExportTopLevel("MetapipeWidget")
class MetapipeWidget(loginUrl: String, logoutUrl: String)
    extends NqsWidget(loginUrl, logoutUrl) {
  override def draftComp = MetapipeDraftComp.apply _
}
