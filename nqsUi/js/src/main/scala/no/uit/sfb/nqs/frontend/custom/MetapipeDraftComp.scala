package no.uit.sfb.nqs.frontend.custom

import io.circe.generic.extras.Configuration
import io.circe.parser
import japgolly.scalajs.react.vdom.VdomNode
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react._
import japgolly.scalajs.react.extra.Ajax
import no.uit.sfb.facade.bootstrap.Spinner
import no.uit.sfb.facade.bootstrap.buttons.Button
import no.uit.sfb.facade.bootstrap.dropdown.{DropdownButton, DropdownItem}
import no.uit.sfb.facade.bootstrap.form.{Form, FormFile, FormGroup}
import no.uit.sfb.facade.bootstrap.grid.{Col, Container, Row}
import no.uit.sfb.facade.icon.Glyphicon
import no.uit.sfb.nqs.frontend.framework.comp._
import no.uit.sfb.nqsshared.models.{FlatNxfConfig, JobManifest}
import org.scalajs.dom
import org.scalajs.dom.raw.HTMLInputElement

import scala.scalajs.js

object MetapipeDraftComp {

  case class Props(jobManifest: JobManifest,
                   updateJob: JobManifest => CallbackTo[Unit],
                   uploadInput: (String, js.Any) => AsyncCallback[Unit],
                   deleteInput: String => Callback,
                   onError: Throwable => Callback,
                   uploading: Set[String]) {
    //The reason why we set defaults here is to make sure that if the user doesn't change the values they will always be included in the config.
    //In addition it is simpler to group all the defaults in one place.
    lazy val nxfConfig = {
      val configWithoutDefaults = FlatNxfConfig(jobManifest.spec.config)
      val defaults = Map(
        "contigsCutoff" -> 1000,
        "trimSettings" -> "AVGQUAL:20 SLIDINGWINDOW:4:15 MINLEN:75",
        "kaiju_refdb" -> "kaiju-mar:1.6-1.5",
        "mapseq_refdb" -> "default",
        "diamond_refdb" -> "diamond-marref-proteins:1.6",
        "diamond_sensitivity" -> "sensitive",
        "interpro_refdb" -> "interpro:5.55-88.0",
        "interpro_apps" -> "TIGRFAM,SMART,ProSiteProfiles,ProSitePatterns,HAMAP,SUPERFAMILY,PRINTS,GENE3D,PIRSF,COILS",
        "priam_refdb" -> "priam:JAN18",
        "removeIncompleteGenes" -> true
      )
      defaults.foldLeft(configWithoutDefaults) {
        case (acc, (k, v)) =>
          //'' is a valid value, so we use the less likely NO_VALUE instead
          if (configWithoutDefaults.getParam(k, "NO_VALUE") == "NO_VALUE") {
            v match {
              case b: Boolean => acc.updateParam(k, b)
              case i: Int     => acc.updateParam(k, i)
              case s: String  => acc.updateParam(k, s)
              case x =>
                throw new Exception(s"Wrong default type: ${x.getClass}")
            }
          } else
            acc
      }
    }
  }

  protected val column = 2
  protected val pipelineOverride = "https://gitlab.com/uit-sfb/metapipe"
  protected val profilesOverride = Seq()

  sealed trait InputModeLike {
    def name: String
    def files: Seq[(String, String)]
    def paramName: String
    def paramValue: String
  }

  //NOTE: in the files mapping below we assume non-gzipped,
  //But this is because we add the file extension later on whe we feed the uploadInput() method
  case object FastqMode extends InputModeLike {
    val name = "R1-R2 FASTQ"
    val files = Seq(
      "Forward reads (FASTQ)" -> "inputs/forward.fastq",
      "Reverse reads (FASTQ)" -> "inputs/reverse.fastq"
    )
    val paramName = "reads"
    val paramValue = "files/inputs/{forward,reverse}.fastq*"
  }
  case object InterleavedFastqMode extends InputModeLike {
    val name = "Interleaved FASTQ"
    val files = Seq("Interleaved reads (FASTQ)" -> "inputs/interleaved.fastq")
    val paramName = "reads"
    val paramValue = "files/inputs/interleaved.fastq*"
  }
  case object FastaMode extends InputModeLike {
    val name = "Contigs"
    val files = Seq("Contigs (FASTA)" -> "inputs/contigs.fasta")
    val paramName = "contigs"
    val paramValue = "files/inputs/contigs.fasta*"
  }

  object InputModes {
    def apply() = Seq(FastqMode, InterleavedFastqMode, FastaMode)
  }

  case class State(inputMode: InputModeLike = FastqMode,
                   versions: Seq[String] = Seq())

  class Backend(val $ : BackendScope[Props, State]) {

    //For some reason, it was not possible to chain two updateJob callbacks (using >> or map{_ => ...})
    //So we group then into one.
    def updateInputModeAndVersion(m: InputModeLike, p: Props, s: State) = {
      val updateParameter = {
        val job = p.jobManifest
        p.updateJob {
          val toDelete = InputModes().filter {
            _.paramName != m.paramName
          }
          val updated = toDelete
            .foldLeft(p.nxfConfig) {
              case (acc, v) =>
                acc.deleteParam(v.paramName)
            }
            .updateParam(m.paramName, m.paramValue)
          job.copy(
            spec = job.spec
              .copy(
                config = updated.cfg,
                version =
                  if (p.jobManifest.spec.version.isEmpty)
                    s.versions.headOption.getOrElse("master")
                  else
                    p.jobManifest.spec.version
              )
          )
        }
      }
      $.modState(s => s.copy(inputMode = m)) >> updateParameter
    }

    def inputsRendering(p: Props, s: State) = {
      def inputForm(label: String, key: String) = {
        val existingKey = Seq(key, s"$key.gz")
          .find(k => p.jobManifest.inputs.exists(_.key == k))
        val isUploading =
          Seq(key, s"$key.gz").exists(k => p.uploading.contains(k))
        val alreadyFilled = existingKey.nonEmpty
        Container()(
          Row()(
            Col(sm = column)(<.div(label, <.br, <.small("Optionally .gz"))),
            Col()(if (isUploading) {
              <.div(<.em("Uploading...  "), Spinner(size = "sm")())
            } else if (alreadyFilled) {
              Button(
                size = "sm",
                variant = "danger",
                onClick = _ => p.deleteInput(existingKey.get)
              )(Glyphicon("Trash"))
            } else {
              Form(onSubmit = _.preventDefaultCB)(
                FormGroup(s"inputFile$key")(
                  FormFile(
                    label = "",
                    onChange = _ =>
                      CallbackTo {
                        val elem = dom.document
                          .getElementById(s"inputFile$key")
                          .asInstanceOf[HTMLInputElement]
                        elem.files(0)
                      }.asAsyncCallback.flatMap { file =>
                        val lastExtension =
                          file.name.split('.').lastOption.getOrElse("")
                        val appendExtension =
                          if (Seq("gz", "gzip", "GZ", "GZIP")
                                .contains(lastExtension))
                            ".gz"
                          else
                            ""
                        p.uploadInput(s"$key$appendExtension", file)
                      }.toCallback,
                    required = true
                  )()
                )
              )
            })
          )
        )
      }
      val modeSelect = DropdownButton(
        id = "mode-select-dropdown",
        title = s.inputMode.name
      )(InputModes().map { m =>
        DropdownItem(onClick = _ => updateInputModeAndVersion(m, p, s))(m.name)
      }: _*)
      <.div(<.h3("Inputs"), modeSelect, <.div(s.inputMode.files.map {
        case (l, f) =>
          inputForm(l, f)
      }: _*))
    }

    def parametersRendering(p: Props, s: State) = {
      val job = p.jobManifest
      val nxfCfg = p.nxfConfig
      def text(label: String,
               param: String,
               default: String = "",
               disabled: Boolean = false) = {
        if (disabled)
          <.div()
        else
          TextField(
            nxfCfg.getParam(param, default),
            label,
            onChange = str => {
              p.updateJob(
                job.copy(
                  spec = job.spec
                    .copy(config = nxfCfg.updateParam(param, str).cfg)
                )
              )
            },
            column = column
          )()
      }
      def posInteger(label: String,
                     param: String,
                     default: Int = 0,
                     disabled: Boolean = false) = {
        if (disabled)
          <.div()
        else
          TextField(
            nxfCfg.getParam(param, default.toString),
            label,
            `type` = "number",
            onChange = str => {
              p.updateJob(
                job.copy(
                  spec = job.spec
                    .copy(config = nxfCfg.updateParam(param, str.toInt).cfg)
                )
              )
            },
            validate = Some(
              str =>
                str.toIntOption match {
                  case Some(x) if x < 0 =>
                    Some(s"Must be a positive integer or zero.")
                  case None => Some(s"Must be an integer.")
                  case _    => None
              }
            ),
            column = column
          )()
      }
      def switch(label: String,
                 param: String,
                 default: Boolean = false,
                 invert: Boolean = false,
                 disabled: Boolean = false) = {
        if (disabled)
          <.div()
        else
          SwitchField(
            nxfCfg.getParam(param, default.toString).toBoolean == !invert,
            label,
            onChange = b => {
              p.updateJob(
                job.copy(
                  spec = job.spec
                    .copy(
                      config = nxfCfg
                        .updateParam(
                          param,
                          if (invert) !b
                          else b
                        )
                        .cfg
                    )
                )
              )
            },
            column = column
          )()
      }

      //Default is head
      def dropdown(label: String,
                   param: String,
                   opts: Seq[(String, String)],
                   disabled: Boolean = false) = {
        if (disabled)
          <.div()
        else
          SelectField(
            nxfCfg.getParam(param, opts.head._1),
            opts.map { case (k, v) => SelectOption(k, v) },
            label,
            onChange = key => {
              p.updateJob(
                job.copy(
                  spec = job.spec
                    .copy(config = nxfCfg.updateParam(param, key).cfg)
                )
              )
            },
            column = column
          )()
      }
      <.div(
        //Do not set defaults below: use the defaults val in Props!
        <.h3("Parameters"),
        text(
          "Trimmomatic settings",
          "trimSettings",
          disabled = s.inputMode == FastaMode
        ),
        posInteger("Contigs cutoff", "contigsCutoff"),
        switch("Binning", "noBinning", invert = true),
        switch("Taxonomic classification", "noTaxo", invert = true),
        dropdown(
          "Kaiju",
          "kaiju_refdb",
          Seq(
            "kaiju-mar:1.6-1.5" -> "MarRef 1.6 & MarDB 1.5",
            "kaiju-refseq:211" -> "RefSeq v211",
            "" -> "Disabled"
          ),
          nxfCfg.getParam("noTaxo", "false").toBoolean
        ),
        dropdown(
          "MapSeq",
          "mapseq_refdb",
          Seq("default" -> "Default", "" -> "Disabled"),
          nxfCfg.getParam("noTaxo", "false").toBoolean
        ),
        switch("Functional assignment", "noFunc", invert = true),
        switch(
          "Discard incomplete genes",
          "removeIncompleteGenes",
          disabled = nxfCfg.getParam("noFunc", "false").toBoolean
        ),
        dropdown(
          "Diamond",
          "diamond_refdb",
          Seq(
            "diamond-marref-proteins:1.6" -> "MarRef 1.6",
            "diamond-uniref50:2022-01" -> "UniRef50 2022-01",
            "" -> "Disabled"
          ),
          nxfCfg.getParam("noFunc", "false").toBoolean
        ),
        dropdown(
          "Diamond sensitivity",
          "diamond_sensitivity",
          Seq("sensitive" -> "Sensitive", "more-sensitive" -> "More sensitive"),
          nxfCfg.getParam("noFunc", "false").toBoolean
        ),
        dropdown(
          "Interpro",
          "interpro_refdb",
          Seq("interpro:5.55-88.0" -> "Interpro 88.0", "" -> "Disabled"),
          nxfCfg.getParam("noFunc", "false").toBoolean
        ),
        text(
          "Interpro apps",
          "interpro_apps",
          disabled = nxfCfg
            .getParam("noFunc", "false")
            .toBoolean || nxfCfg.getParam("interpro_refdb", "").isEmpty
        ),
        dropdown(
          "Priam",
          "priam_refdb",
          Seq("priam:JAN18" -> "JAN18", "" -> "Disabled"),
          nxfCfg.getParam("noFunc", "false").toBoolean
        )
      )
    }

    def render(p: Props, s: State): VdomNode = {
      def versionLabel(v: String) = if (v == "master") "BETA" else v
      val job = p.jobManifest
      val versionSelect = DropdownButton(
        id = "version-select-dropdown",
        title = versionLabel(job.spec.version)
      )((s.versions.headOption.toSeq :+ "master").map { v =>
        DropdownItem(
          onClick = _ =>
            p.updateJob(job.copy(spec = job.spec.copy(version = v)))
        )(versionLabel(v))
      }: _*)
      <.div(
        Seq(
          <.h2("Metapipe"),
          versionSelect,
          TextField(
            job.jobId,
            "Job id",
            disabled = Some(""),
            column = column
          )(),
          TextField(
            job.spec.label,
            "Job name",
            smallPrint =
              "Give a meaningful name for this job. (Ex: input file name, ...)",
            validate = Some(
              str =>
                if (str.isEmpty) Some("Job name may not be empty.")
                else None
            ),
            onChange =
              str => p.updateJob(job.copy(spec = job.spec.copy(label = str))),
            column = column
          )()
        ) ++
          Seq(inputsRendering(p, s), parametersRendering(p, s)): _*
      )
    }
  }

  protected def loadVersions()(
    onError: Throwable => Callback
  ): AsyncCallback[Seq[String]] = {
    implicit val customConfig: Configuration =
      Configuration.default.withDefaults
    Ajax("GET", "https://gitlab.com/api/v4/projects/18975308/releases").setRequestContentTypeJson.send.asAsyncCallback
      .map { xhr =>
        parser
          .parse(xhr.responseText)
          .toOption
          .flatMap { json =>
            json.asArray
          }
          .map { arr =>
            arr
              .flatMap {
                _.asObject
              }
              .flatMap { json =>
                json("tag_name")
              }
              .flatMap { _.asString }
          }
          .getOrElse(Seq())
      }
  }

  private val component =
    ScalaComponent
      .builder[Props]
      .initialStateFromProps { p =>
        val m = InputModes()
          .find(m => m.paramValue == p.nxfConfig.getParam(m.paramName))
          .getOrElse(FastqMode)
        State(m)
      }
      .renderBackend[Backend]
      .componentDidMount(lf => {
        val p = lf.props
        val s = lf.state
        loadVersions()(p.onError)
          .flatMap {
            versions =>
              lf.modStateAsync { s =>
                  s.copy(versions = versions)
                }
                .flatMap {
                  _ =>
                    //Yes, it is necessary to set versions in s because even though we use flatMap,
                    //since we are in componentDidMount, it would use the original s without the versions.
                    lf.backend
                      .updateInputModeAndVersion(
                        s.inputMode,
                        p,
                        s.copy(versions = versions)
                      )
                      .asAsyncCallback
                }
          }
      }.toCallback)
      .build

  def apply(jobManifest: JobManifest,
            updateJob: JobManifest => Callback,
            uploadInput: (String, js.Any) => AsyncCallback[Unit],
            deleteInput: String => Callback,
            onError: Throwable => Callback,
            uploading: Set[String]) = {
    def jmUpdate(jm: JobManifest) = {
      val withOverrides = jm.copy(
        spec =
          jm.spec.copy(pipeline = pipelineOverride, profiles = profilesOverride)
      )
      updateJob(withOverrides)
    }
    component(
      Props(jobManifest, jmUpdate, uploadInput, deleteInput, onError, uploading)
    )
  }
}
