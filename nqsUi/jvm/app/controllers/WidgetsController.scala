package controllers

import modules.Config
import modules.auth.{Authorizer, SecurityLike}
import org.pac4j.play.scala.SecurityComponents
import play.api.Logging
import play.api.mvc._

import javax.inject._

@Singleton
class WidgetsController @Inject()(val controllerComponents: SecurityComponents,
                                  implicit val authorizer: Authorizer,
                                  implicit val cfg: Config)
    extends SecurityLike
    with Logging {

  def nqsWidget(sec: Boolean) = {
    (if (sec)
       secure
     else
       open) { implicit request: Request[AnyContent] =>
      Ok(views.html.NqsWidget(cfg.apiUrl, authorizer.user.flatMap {
        _.accessToken
      }, cfg.id, cfg.nxfConfigOverride))
    }
  }
}
