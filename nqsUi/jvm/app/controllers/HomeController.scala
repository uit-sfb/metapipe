package controllers

import javax.inject._
import modules.Config
import modules.auth.{Authorizer, SecurityLike}
import play.api.libs.json.Json
import play.api.Logging
import play.api.mvc._
import no.uit.sfb.info.nqsui.BuildInfo
import no.uit.sfb.nqsshared.models.auth.UserInfo
import no.uit.sfb.nqsshared.models.generic.Info
import org.pac4j.play.scala.SecurityComponents
import play.api.libs.concurrent.Futures

import scala.concurrent.ExecutionContext

@Singleton
class HomeController @Inject()(val controllerComponents: SecurityComponents,
                               implicit val authorizer: Authorizer,
                               implicit val f: Futures,
                               implicit val cfg: Config,
                               implicit val ec: ExecutionContext)
    extends SecurityLike
    with Logging {

  def index() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index(BuildInfo.version))
  }

  def info() = Action {
    val res =
      Info(BuildInfo.name, BuildInfo.version, BuildInfo.gitCommit, false)
    implicit val writer = Json.writes[Info]
    Ok(Json.toJson(res))
  }

  def login() = secure { implicit request: Request[AnyContent] =>
    println(
      s"Connected as ${authorizer.user.map { _.id }.getOrElse("anonymous")}"
    )
    Ok(views.html.login())
  }

  def userInfo(): Action[AnyContent] =
    open { implicit request: Request[AnyContent] =>
      implicit val writer = Json.writes[UserInfo]
      Ok(Json.toJson(authorizer.user match {
        case Some(user) =>
          UserInfo(user.id, user.email, "none")
        case None =>
          UserInfo("", role = "anonymous")
      }))
    }

  //Do NOT secure this method
  def connected(): Action[AnyContent] = open {
    implicit request: Request[AnyContent] =>
      Ok(authorizer.isLoggedIn.toString)
  }
}
