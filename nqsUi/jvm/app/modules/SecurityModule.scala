package modules

import com.google.inject.{AbstractModule, Provides}
import com.typesafe.scalalogging.LazyLogging
import org.pac4j.core.client.Clients
import org.pac4j.core.client.direct.AnonymousClient
import org.pac4j.core.config.Config
import org.pac4j.core.context.WebContext
import org.pac4j.core.credentials.TokenCredentials
import org.pac4j.core.http.url.DefaultUrlResolver
import org.pac4j.core.profile.UserProfile
import org.pac4j.core.profile.creator.ProfileCreator
import org.pac4j.jwt.credentials.authenticator.JwtAuthenticator
import org.pac4j.oidc.client.OidcClient
import org.pac4j.oidc.config.OidcConfiguration
import org.pac4j.play.http.PlayHttpActionAdapter
import org.pac4j.play.scala.{DefaultSecurityComponents, SecurityComponents}
import org.pac4j.play.store.{PlayCacheSessionStore, PlaySessionStore}
import org.pac4j.play.{CallbackController, LogoutController}
import play.api.{Configuration, Environment}

import java.util.Optional
import scala.util.Try

/**
  * Guice DI module to be included in application.conf
  */
//Do NOT remove Environment!
class SecurityModule(environment: Environment, configuration: Configuration)
    extends AbstractModule
    with LazyLogging {

  override def configure(): Unit = {
    bind(classOf[PlaySessionStore]).to(classOf[PlayCacheSessionStore])

    bind(classOf[SecurityComponents]).to(classOf[DefaultSecurityComponents])

    // callback
    val callbackController = new CallbackController()
    callbackController.setRenewSession(false)
    bind(classOf[CallbackController]).toInstance(callbackController)

    // logout
    val logoutController = new LogoutController()
    //For some reason, the default url is not inserted in the call to the oidc server (post_logout_redirect_uri parameter) when doing a central logout
    logoutController.setCentralLogout(true)
    //Needs to be an absolute path. To override it, add url=/my/logout/path to the call to /logout route
    logoutController.setDefaultUrl("/")
    bind(classOf[LogoutController]).toInstance(logoutController)
  }

  protected def anonymousClient: Option[AnonymousClient] = {
    val client = new AnonymousClient()
    client.setName("AnonymousClient")
    Some(client)
  }

  protected def elixirAaiClient: Option[OidcClient[OidcConfiguration]] = {
    val oidcConfiguration = new OidcConfiguration()
    //oidcConfiguration.setScope("openid profile email")
    Try {
      oidcConfiguration.setClientId(
        configuration.get[String]("app.auth.users.elixirAai.client.id")
      )
      oidcConfiguration.setSecret(
        configuration.get[String]("app.auth.users.elixirAai.client.secret")
      )
      oidcConfiguration.setDiscoveryURI(
        configuration.get[String]("app.auth.users.elixirAai.discovery")
      )
      oidcConfiguration.setPreferredJwsAlgorithm("RS256")
      oidcConfiguration.setScope("openid email profile")
      //This setting MUST remain commented out so that the prompt mode is automatically set to the needed value.
      //oidcConfiguration.addCustomParam("prompt", "consent") //'consent' or 'none'
      oidcConfiguration.setExpireSessionWithToken(false)
    }.toOption match {
      case Some(_) =>
        val oidcClient =
          new OidcClient[OidcConfiguration](oidcConfiguration)
        oidcClient.setName("ElixirAaiClient")
        Some(oidcClient)
      case None =>
        logger.warn(
          s"Could not create security client ElixirAaiClient due to configuration error."
        )
        None
    }
  }

  @Provides
  def provideConfig()(): Config = {
    val cls =
      Seq(anonymousClient, elixirAaiClient).flatten
    val clients = new Clients(cls: _*)
    val selfEndpoint =
      configuration.getOptional[String]("app.selfEndpoint").getOrElse("")
    val prefixPath = {
      val p = configuration.get[String]("play.http.context")
      if (p.endsWith("/"))
        p.dropRight(1)
      else
        p
    }
    val selfUrl = selfEndpoint + prefixPath
    //When using a relative Url with completeRelativeUrl set to true, it seems that the web context indicates http when it should be https (most likely due to the reverse proxy)
    clients.setCallbackUrl(s"$selfUrl/callback")
    clients.setUrlResolver(new DefaultUrlResolver(selfEndpoint.isEmpty))
    val config = new Config(clients)
    config.setHttpActionAdapter(new PlayHttpActionAdapter())
    config
  }
}

class JwtProfileCreator(authenticator: JwtAuthenticator)
    extends ProfileCreator[TokenCredentials] {
  def create(credentials: TokenCredentials,
             context: WebContext): Optional[UserProfile] = {
    Optional.ofNullable[UserProfile](
      authenticator.validateToken(credentials.getToken)
    )
  }
}
