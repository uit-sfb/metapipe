package modules

import com.typesafe.scalalogging.LazyLogging
import play.api.Configuration

import java.nio.file.Paths
import javax.inject._
import scala.concurrent.ExecutionContext
import scala.io.Source
import scala.util.{Failure, Success, Try}

@Singleton
class Config @Inject()(cfg: Configuration)(
  implicit executionContext: ExecutionContext
) extends LazyLogging {
  val id = cfg.get[String]("app.id")
  val selfEndpoint = cfg.getOptional[String]("app.selfEndpoint").getOrElse("")
  val prefixPath = {
    val p = cfg.get[String]("play.http.context")
    if (p.endsWith("/"))
      p.dropRight(1)
    else
      p
  }
  val selfUrl = selfEndpoint + prefixPath
  val apiUrl = cfg.get[String]("app.apiUrl")
  protected val nxfConfigOverridePath = cfg.get[String]("app.nxfConfigOverride")
  val nxfConfigOverride = Try {
    Source.fromFile(Paths.get(nxfConfigOverridePath).toFile)
  }.recoverWith { case _ => Try { Source.fromResource(nxfConfigOverridePath) } } match {
    case Success(src) =>
      val str = src.mkString
      src.close()
      str
    case Failure(e) =>
      ""
  }
  logger.info(s"""
       |id: '$id'
       |selfUrl: '$selfUrl'
       |apiUrl: '$apiUrl'
       |""".stripMargin)
}
