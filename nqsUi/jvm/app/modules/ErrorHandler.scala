package modules

import com.typesafe.scalalogging.LazyLogging
import models.json.JsonApi._
import no.uit.sfb.nqsshared.com.error.{
  ErrorLike,
  GatewayTimeoutError,
  InternalServerErrorError
}
import no.uit.sfb.nqsshared.jsonapi._
import play.api.http.HttpErrorHandler
import play.api.libs.json.Json
import play.api.mvc.Results._
import play.api.mvc._

import java.util.concurrent.CompletionException
import javax.inject.Singleton
import scala.concurrent._

@Singleton
class ErrorHandler extends HttpErrorHandler with LazyLogging {
  def onClientError(request: RequestHeader,
                    statusCode: Int,
                    message: String): Future[Result] = {
    Future.successful {
      logger.error(s"An error occurred ($statusCode): $message")
      Status(statusCode)(
        Json.toJson(
          ErrorResponse(
            Seq(Err(statusCode.toString, "A client error occurred", message))
          )
        )
      )
    }
  }

  def onServerError(request: RequestHeader,
                    exception: Throwable): Future[Result] = {
    exception match {
      case e: CompletionException =>
        onServerError(request, e.getCause)
      case e: ExecutionException => //Boxed Error
        onServerError(request, e.getCause)
      case e: ErrorLike =>
        logger.error("An error occurred:", e)
        Future.successful(
          Status(e.statusCode)(Json.toJson(ErrorResponse(Seq(e.toErr))))
            .as(ResponseLike.contentType)
        )
      case e: TimeoutException =>
        logger.error("A timeout error occurred:", e)
        e.printStackTrace()
        onServerError(request, GatewayTimeoutError(e.getMessage))
      case e: Throwable =>
        logger.error("An unexpected error occurred:", e)
        e.printStackTrace()
        onServerError(request, InternalServerErrorError(e.getMessage))
    }
  }
}
