package modules.auth

import models.auth.UserDetails
import org.pac4j.core.authorization.authorizer.DefaultAuthorizers
import org.pac4j.core.profile.CommonProfile
import org.pac4j.play.scala.Pac4jScalaTemplateHelper
import play.api.mvc._
import play.api.{Configuration, Logging}

import javax.inject._
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class Authorizer @Inject()(
  parser: BodyParsers.Default,
  implicit val ec: ExecutionContext,
  implicit val pac4jTemplateHelper: Pac4jScalaTemplateHelper[CommonProfile],
  implicit val cfg: Configuration
) extends Logging {
  protected val _directClients = Seq()
  //Note: may contain Anonymous client, which we consider as an indirect client for our purpose (bypassing security)
  protected val _indirectClients = Seq(cfg.get[String]("app.auth.users.client"))

  //Note: When making an AJAX call, we set the header 'X-Requested-With: XMLHttpRequest',
  //which tells pac4j that it should NOT use any indirect client (since the request is AJAX)
  //Attention: Indirect clients must be BEFORE direct clients
  val clients = (_indirectClients ++ _directClients).mkString(",")

  val bypassSecurity = _indirectClients.contains("AnonymousClient")

  val authMode =
    if (bypassSecurity)
      DefaultAuthorizers.IS_ANONYMOUS
    else
      DefaultAuthorizers.IS_AUTHENTICATED

  //Checks the header "Authorization" to decide whether to use Action or Secure
  def openActionBuilder(
    secureActionBuilder: ActionBuilder[Request, AnyContent],
    insecureActionBuilder: ActionBuilder[Request, AnyContent]
  ) =
    new ActionBuilderImpl[AnyContent](parser) with Logging {
      override def invokeBlock[A](
        request: Request[A],
        block: Request[A] => Future[Result]
      ): Future[Result] = {
        val actionBuilder =
          if (request.headers.hasHeader("Authorization"))
            secureActionBuilder
          else
            insecureActionBuilder
        actionBuilder.invokeBlock[A](request, block)
      }
    }

  def isLoggedIn(implicit request: RequestHeader): Boolean =
    user.nonEmpty

  def user(implicit request: RequestHeader): Option[UserDetails] = {
    pac4jTemplateHelper.getCurrentProfile
      .map { UserDetails(_) }
      .flatMap { user =>
        if (user.id.isEmpty) {
          if (bypassSecurity)
            Some(user.copy(id = "anonymous"))
          else
            None
        } else
          Some(user)
      }
  }
}
