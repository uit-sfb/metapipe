package modules.auth

import org.pac4j.core.profile.CommonProfile
import org.pac4j.play.scala.Security

trait SecurityLike extends Security[CommonProfile] {
  def authorizer: Authorizer

  val secure = Secure(authorizer.clients, authorizers = authorizer.authMode)

  //Use this ActionBuilder instead of Action when no Authorization is strictly needed,
  //but nice to do in case the Authorization header is present in cases where the Action uses the authorizer
  val open = authorizer.openActionBuilder(secure, Secure("AnonymousClient"))
}
