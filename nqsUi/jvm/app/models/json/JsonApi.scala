package models.json

import play.api.libs.json._
import io.circe.parser._
import io.circe.generic.extras.auto._
import io.circe.generic.extras.Configuration
import io.circe.syntax._
import no.uit.sfb.nqsshared.jsonapi._

object JsonApi {

  implicit private val customConfig: Configuration =
    Configuration.default.withDefaults

  implicit val config = JsonConfiguration(
    optionHandlers = OptionHandlers.WritesNull
  )

  implicit val mapOptionFormat = new Format[Map[String, Option[String]]] {
    def writes(obj: Map[String, Option[String]]): JsValue =
      Json.parse(obj.asJson.noSpaces)

    def reads(json: JsValue): JsResult[Map[String, Option[String]]] = {
      JsResult.fromTry(
        decode[Map[String, Option[String]]](json.toString()).toTry
      )
    }
  }

  implicit val formatRelationship =
    Json.using[Json.WithDefaultValues].format[Relationship]
  implicit val formatRelationships =
    Json.using[Json.WithDefaultValues].format[Relationships]
  implicit val formatErr = Json.using[Json.WithDefaultValues].format[Err]
  implicit val formatErrorResponse =
    Json.using[Json.WithDefaultValues].format[ErrorResponse]
}
