# NQS UI

A user interface can be added with [NQS](https://gitlab.com/uit-sfb/nextflow-queuer).
Refer to the [NQS Wiki pages](https://gitlab.com/uit-sfb/nextflow-queuer/-/wikis/home) for more details.

## Getting started

- `sbt`
- `project nqsUIJVM`
- `run 9002`

or use the Docker image.