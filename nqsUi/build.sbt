import com.typesafe.sbt.packager.docker.DockerPlugin.autoImport.{dockerBaseImage, dockerRepository, dockerUsername}
import sbt.Keys.publishConfiguration
import sbtbuildinfo.BuildInfoPlugin.autoImport.{buildInfoKeys, buildInfoPackage}
import sbtcrossproject.CrossProject

ThisBuild / scalaVersion := "2.13.8"
ThisBuild / organization     := "no.uit.sfb"
ThisBuild / organizationName := "uit-sfb"
ThisBuild / maintainer := "mma227@uit.no"

lazy val gitCommit = settingKey[String]("Git SHA")
lazy val gitRefName = settingKey[String]("Git branch or tag")
//lazy val isRelease = settingKey[Boolean]("Is release")
lazy val dockerRegistry = settingKey[String]("Docker registry")
lazy val dockerImageName = settingKey[String]("Docker image name")

useJGit

ThisBuild / gitCommit := { sys.env.getOrElse("CI_COMMIT_SHA", "unknown")}
ThisBuild / gitRefName := { sys.env.getOrElse("CI_COMMIT_REF_NAME", git.gitCurrentBranch.value) }
//ThisBuild / isRelease := { sys.env.getOrElse("CI_COMMIT_REF_PROTECTED", "false").toBoolean && sys.env.getOrElse("CI_COMMIT_TAG", "").nonEmpty}
ThisBuild / dockerRegistry := { sys.env.getOrElse("CI_REGISTRY", s"registry.gitlab.com") }
ThisBuild / dockerImageName := {
  s"${sys.env.getOrElse("CI_REGISTRY_IMAGE", s"${dockerRegistry.value}/${organizationName.value}/metapipe-ui")}"
}
ThisBuild / version := gitRefName.value //Do not use slug here as it would replace '.' with '-'

ThisBuild / scalacOptions ++= Seq(
  "-feature",
  "-deprecation"
)
ThisBuild / resolvers += "gitlab" at "https://gitlab.com/api/v4/groups/2067095/-/packages/maven"

lazy val commonDep = Seq("org.scalatest" %% "scalatest" % "3.2.11" % Test)
val pac4jVersion = "4.5.3"
val nqsVersion = "master"

lazy val root = project.in(file("."))
  .aggregate(nqsUI.jvm, nqsUI.js)
  .settings(
    publish := {},
    publishLocal := {},
  )

lazy val nqsUI: CrossProject = crossProject(JSPlatform, JVMPlatform).in(file("."))
  .settings(
    libraryDependencies ++= Seq(
      "no.uit.sfb" %%% "nqsserver" % nqsVersion
    ),
    useCoursier := false,
    publishConfiguration := publishConfiguration.value.withOverwrite(true), //Always allow overwrite since Scala API documentation gets published by docker:publish somehow
  )
  .jvmSettings(
    libraryDependencies ++= commonDep ++ Seq(
      "com.vmunier" %% "scalajs-scripts" % "1.2.0",
      guice,
      caffeine,
      ws,
      specs2 % Test,
      "org.scalatestplus.play" %% "scalatestplus-play" % "5.1.0" % Test,
      "com.typesafe" % "config" % "1.4.2",
      "org.pac4j" %% "play-pac4j" % "10.0.2",
      "org.pac4j" % "pac4j-http" % pac4jVersion,
      "org.pac4j" % "pac4j-jwt" % pac4jVersion,
      "org.pac4j" % "pac4j-oidc" % pac4jVersion exclude("commons-io", "commons-io"), //Used by JWT
      "org.kie.modules" % "org-apache-commons-compress" % "6.5.0.Final",
      "org.jetbrains.kotlinx" % "kotlinx-datetime" % "0.3.2",
      "com.typesafe.scala-logging" %% "scala-logging" % "3.9.4",
      "ch.qos.logback" % "logback-classic" % "1.2.11",
    ),
    dockerLabels := Map("gitCommit" -> s"${gitCommit.value}@${gitRefName.value}"),
    dockerLabels := Map("gitCommit" -> s"${gitCommit.value}@${gitRefName.value}"), //Do NOT add in Docker
    Docker / dockerRepository := Some(dockerRegistry.value),
    Docker / dockerUsername := Some(dockerImageName.value.split("/").tail.mkString("/")),
    dockerBaseImage := "openjdk:11", //Do NOT add "in Docker"
    //dockerChmodType in Docker := DockerChmodType.UserGroupWriteExecute,
    //dockerPermissionStrategy in Docker := DockerPermissionStrategy.CopyChown,
    //daemonGroup in Docker := "0",
    Docker / daemonUser := "sfb", //"in Docker" needed for this parameter,
    Docker / maintainer := (ThisBuild / maintainer).value,
    scalaJSProjects := Seq(nqsUI.js),
    Assets / pipelineStages := Seq(scalaJSPipeline),
    pipelineStages := Seq(digest, gzip),
    // triggers scalaJSPipeline when using compile or continuous compilation
    Compile / compile := ((Compile / compile) dependsOn scalaJSPipeline).value,
    buildInfoKeys := Seq[BuildInfoKey](
      name,
      version,
      scalaVersion,
      sbtVersion,
      gitCommit
    ),
    buildInfoPackage := s"${organization.value}.info.${name.value.toLowerCase.replace('-', '_')}",
    //buildInfoOptions += BuildInfoOption.BuildTime
  )
  .jvmConfigure(_.enablePlugins(PlayScala, DockerPlugin, JavaAppPackaging, BuildInfoPlugin, WebScalaJSBundlerPlugin)
  )
  .jsSettings(
    scalaJSUseMainModuleInitializer := false,
    webpackBundlingMode := BundlingMode.LibraryAndApplication(), //https://stackoverflow.com/a/64317357/4965515
    webpack / version := "4.46.0", //Cannot update to 5+
    startWebpackDevServer / version := "3.11.2",
    //webpackConfigFile := Some(baseDirectory.value / "webpack.config.js"),
    Compile / npmDependencies ++= Seq(
      "react" -> "17.0.1",
      "react-dom" -> "17.0.1",
      "react-bootstrap" -> "1.6.0",
      "react-icons" -> "4.2.0",
      "jsdom" -> "16.5.3"
    ),
    Test / npmDependencies ++= Seq(),
    Test / requireJsDomEnv := true
  )
  .enablePlugins(ScalaJSBundlerPlugin)

//Automatic reload
Global / onChangedBuildSource := ReloadOnSourceChanges