# Wordpress plugins

## How to load a plugin to Wordpress?

- Download the
  plugin: `curl https://gitlab.com/api/v4/projects/18975308/packages/generic/metapipe_wordpress/<version>/metapipe_wordpress.zip --output metapipe_wordpress.zip`
  , where `<version>` is one of the versions listed [here](https://gitlab.com/uit-sfb/metapipe/-/releases)
- Log in to Wordpress and go to `Plugins` -> `Add New` -> `Upload Plugin` -> `Install Now`
- Select `metapipe_wordpress.zip`
- Click on `Activate`

## Troubleshooting

If the URL are not resolved properly, it might be that the rules defined by the plugin were not picked up by Wordpress.
To fix this, go to `Settings` -> `Permalinks` then click on `Save` without doing any modifications.