# Metapipe Widget

Add shortcodes to display Metapipe widgets.

## Getting started

- Install and activate the plugin (see [here](../README.md)).
- Set up the plugin: Go to Metapipe and fill up the fields:
    - `NQS service URL`: URL of the NQS service endpoint
- Add the shortcodes described below where appropriate.

## Shortcodes

### [MetapipeJobs]

Display the job list widget.

#### Attributes

None

Example: `[MetapipeJobs]`
