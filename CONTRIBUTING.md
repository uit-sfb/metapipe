# Contributing

Metapipe is an opensource [Nextflow](https://nextflow.io) pipeline, and as such
we are welcoming contributions from anyone.
This includes:
- update of tools / reference databases
- addition of tools / reference databases
- bug fixes

## Guidelines

Before submitting a PR, please ensure that:
- `nextflow run ../.. -profile test -with-trace --refdbDir /home/.metapipe/refdb` works.
- You have updated (when needed):
  - the [schematic](resources/images/schematics/pipeline.png)
  - the help text in [main.nf](main.nf) with any new parameter / database version etc.
  - the [default config](nextflow.config)
  - the [UI](nqsUi/js/src/main/scala/no/uit/sfb/nqs/frontend/custom/MetapipeDraftComp.scala)
  - the [production configuration](nqsUi/jvm/conf/metapipe.config)