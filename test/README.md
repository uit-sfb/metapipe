# Testing

From this directory, run:

```
mkdir -p target && cd target
```
 Then:
- with Docker: `nextflow run ../.. -profile test -with-trace --refdbDir /home/.metapipe/refdb`
- with Singularity: `nextflow run ../.. -profile testSingularity -with-trace --refdbDir /home/.metapipe/refdb`

Note:  
When running locally, version `master` is used for the Docker images.
If you need to run locally but with a specific version for the containers, add the following lines to `nextflow.config`:

```
manifest {
  version = "x.y.z"
}
```