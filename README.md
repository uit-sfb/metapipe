# META-pipe

[META-pipe](https://gitlab.com/uit-sfb/metapipe) is a [Nextflow](https://www.nextflow.io/) workflow for analysis and annotation of metagenomic samples,
providing insight into phylogenetic diversity, as well as metabolic and functional properties of environmental communities.

The workflow was [designed](https://munin.uit.no/handle/10037/11180) to leverage the most modern tools and reference databases to 
ensure both quality outputs and efficient resource usage and is divided into four modules:
  - filtering and assembly
  - taxonomic classification
  - functional assignment
  - binning

![Metapipe workflow](resources/images/schematics/collage.svg)  

## META-pipe service

Metapipe is provided as an [Elixir service](https://mmp2.sfb.uit.no/metapipe).
Feel free to submit your metagenomes for processing!

## Documentation

The documentation is located in the project's [Wiki pages](https://gitlab.com/uit-sfb/metapipe/-/wikis/home).
