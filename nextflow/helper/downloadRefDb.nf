params.refdbDir = "${launchDir}"

process DownloadRefDb {
  label 'helper'

  container "registry.gitlab.com/uit-sfb/metapipe/ref-db:${workflow.manifest.version ?: workflow.revision ?: 'master'}"
  containerOptions = { if (workflow.containerEngine == "docker") "-v ${params.refdbDir}:/refdb" else if (workflow.containerEngine == "singularity") "-B ${params.refdbDir}:/refdb" else "" }

  input:
    val refDb

  output:
    env dbPath, emit: dbPath
    env extDbPath, emit: extDbPath

  when: refDb != true

  shell:
    '''
    set +u
    case $(echo "!{task.memory}" | cut -d' ' -f2) in
         [gG]B*) UNIT=1073741824;;
         [mM]B*) UNIT=1048576;;
         [kK]B*) UNIT=1024;;
         B*) UNIT=1;;
    esac
    MEMORY=$(( $(echo "!{task.memory}" | cut -d '.' -f1 | cut -d ' ' -f1) * $UNIT ))
    IFS=':' read -ra DB_SPLIT <<< "!{refDb}"
    DB_NAME="${DB_SPLIT[0]}"
    DB_VERSION="${DB_SPLIT[1]}"
    if [[ -n "$DB_NAME" ]]; then
      if [[ -z "$DB_VERSION" ]]; then
        DB_VERSION=$(/opt/docker/bin/ref-db -- latest "$DB_NAME")
      fi
      dbPath="/refdb/$DB_NAME/$DB_VERSION"
      extDbPath="!{params.refdbDir}/$DB_NAME/$DB_VERSION"
      set -x
      id
      ls -la /refdb
      /opt/docker/bin/ref-db -XX:ActiveProcessorCount=!{task.cpus} -J-Xms$((MEMORY/2)) -J-Xmx$MEMORY -- download -d /refdb ${DB_NAME}=${DB_VERSION}
    fi
    '''
}
