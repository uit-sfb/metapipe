params.export = '.'

process Export {
  label 'helper'

  publishDir "${params.export}", mode: 'link'

  input:
    val s
    path p, stageAs: "out/*"
    val compress

  output:
    path "${s}" optional true
    path "${s}.tar.gz" optional true

  shell:
    '''
    mkdir -p "!{s}"
    mv out/* "!{s}"
    if [ "!{compress}" == "true" ]; then
      tar -hczf "!{s}.tar.gz" "!{s}"
      rm -R "!{s}"
    fi
    '''
}
