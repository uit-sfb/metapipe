include {DesinterleaveReads} from './process/desinterleaveReads.nf'
include {Seqprep} from './process/seqprep.nf'
include {TrimmomaticPE} from './process/trimmomatic.nf' params(settings: params.trimSettings)
include {Rrnapred} from './process/rrnapred.nf'
include {PairReads} from './process/pairReads.nf'
include {Megahit} from './process/megahit.nf' params(contigsCutoff: params.contigsCutoff)
include {Export} from '../helper/export.nf'

workflow Assembly {
  take:
    read_pairs_ch

  main:
    DesinterleaveReads(read_pairs_ch)
    TrimmomaticPE(DesinterleaveReads.out.r1, DesinterleaveReads.out.r2)
    TrimmomaticPE.out.r1.mix(TrimmomaticPE.out.r2).map{ path -> tuple(path.simpleName, path) } | Rrnapred
    pred16s = Rrnapred.out.r1_pred16s.concat(Rrnapred.out.r2_pred16s).collectFile(name: 'pred16s.fasta', newLine: false)
    PairReads(Rrnapred.out.r1_filtered, Rrnapred.out.r2_filtered) | flatten | map { path -> tuple(path.baseName, path) } | Seqprep
    unmergedR1 = Seqprep.out.unmergedR1.collectFile(sort: {it.parent.baseName})
    unmergedR2 = Seqprep.out.unmergedR2.collectFile(sort: {it.parent.baseName})
    merged = Seqprep.out.merged.collectFile(sort: {it.parent.baseName})
    Megahit(unmergedR1, unmergedR2, merged)
    export_ch = pred16s.mix(Megahit.out.contigs) | collect
    Export("assembly", export_ch, false)

  emit:
    trimmedR1 = TrimmomaticPE.out.r1
    trimmedR2 = TrimmomaticPE.out.r2
    pred16s = pred16s
    contigs = Megahit.out.contigs
}
