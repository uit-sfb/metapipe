process PairReads {
  label 'assembly'

  container "registry.gitlab.com/uit-sfb/metapipe/preprocess-reads:${workflow.manifest.version ?: workflow.revision ?: 'master'}"

  ext.slices = 1

  input:
    path inputR1, stageAs: 'in/r1.fastq'
    path inputR2, stageAs: 'in/r2.fastq'

  output:
    path 'out/slices/*', emit: slices

  shell:
    '''
    set +u
    case $(echo "!{task.memory}" | cut -d' ' -f2) in
         [gG]B*) UNIT=1073741824;;
         [mM]B*) UNIT=1048576;;
         [kK]B*) UNIT=1024;;
         B*) UNIT=1;;
    esac
    MEMORY=$(( $(echo "!{task.memory}" | cut -d '.' -f1 | cut -d ' ' -f1) * $UNIT ))
    set -x
    /opt/docker/bin/preprocess-reads -J-Xms$((MEMORY/2)) -J-Xmx$MEMORY -XX:ActiveProcessorCount=!{task.cpus} -- \
      --r1 !{inputR1} --r2 !{inputR2} --outputDir out/slices --slices !{task.ext.slices}
    '''
}