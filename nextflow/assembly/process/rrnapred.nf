process Rrnapred {
  label 'assembly'
  tag "${FILENAME}"

  container "registry.gitlab.com/uit-sfb/metapipe/rrnapred:${workflow.manifest.version ?: workflow.revision ?: 'master'}"

  input:
    tuple val(FILENAME), path(input, stageAs: 'in/*')

  output:
    path 'out/trimmedR1/filtered.fastq.gz', emit: r1_filtered, optional: true
    path 'out/trimmedR1/pred16s.fasta', emit: r1_pred16s, optional: true
    path 'out/trimmedR2/filtered.fastq.gz', emit: r2_filtered, optional: true
    path 'out/trimmedR2/pred16s.fasta', emit: r2_pred16s, optional: true

  shell:
    '''
    set +u
    case $(echo "!{task.memory}" | cut -d' ' -f2) in
         [gG]B*) UNIT=1073741824;;
         [mM]B*) UNIT=1048576;;
         [kK]B*) UNIT=1024;;
         B*) UNIT=1;;
    esac
    MEMORY=$(( $(echo "!{task.memory}" | cut -d '.' -f1 | cut -d ' ' -f1) * $UNIT ))
    OUT_SPECIFIC=out/!{FILENAME}
    mkdir -p "$OUT_SPECIFIC"
    set -x
    /opt/docker/bin/rrnapred -J-Xms$((MEMORY/2)) -J-Xmx$MEMORY -XX:ActiveProcessorCount=!{task.cpus} -- \
      -i !{input} --out $OUT_SPECIFIC --cpu !{task.cpus}
    '''
}