process DesinterleaveReads {
  label 'assembly'

  container "registry.gitlab.com/uit-sfb/metapipe/preprocess-reads:${workflow.manifest.version ?: workflow.revision ?: 'master'}"

  ext.slices = 1

  input:
      path input, stageAs: 'in/*'

    output:
      path 'out/r1.fastq.gz', emit: r1
      path 'out/r2.fastq.gz', emit: r2

  shell:
    '''
    set +u
    case $(echo "!{task.memory}" | cut -d' ' -f2) in
         [gG]B*) UNIT=1073741824;;
         [mM]B*) UNIT=1048576;;
         [kK]B*) UNIT=1024;;
         B*) UNIT=1;;
    esac
    MEMORY=$(( $(echo "!{task.memory}" | cut -d '.' -f1 | cut -d ' ' -f1) * $UNIT ))
    #Just a hack around the issue with input splitting up path when only one path
    if [[ "!{input[0]}" != "in" ]]; then
      R1_PARAM="--r1 !{input[0]}"
      R2_PARAM="--r2 !{input[1]}"
    else
      INTERLEAVED_PARAM="--interleaved !{input[0]}/!{input[1]}"
    fi
    set -x
    /opt/docker/bin/preprocess-reads -J-Xms$((MEMORY/2)) -J-Xmx$MEMORY -XX:ActiveProcessorCount=!{task.cpus} -- \
      $R1_PARAM $R2_PARAM $INTERLEAVED_PARAM --outputDir out --slices 0
    '''
}