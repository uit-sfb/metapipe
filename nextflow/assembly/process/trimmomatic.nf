params.settings = ""

process TrimmomaticPE {
  label 'assembly'

  container 'registry.gitlab.com/uit-sfb/genomic-tools/trimmomatic:0.39'

  input:
    path 'in/readsR1.fastq.gz'
    path 'in/readsR2.fastq.gz'

  output:
    path 'out/trimmedR1.fastq.gz', emit: r1
    path 'out/trimmedR2.fastq.gz', emit: r2

  shell:
    '''
    set +u
    case $(echo "!{task.memory}" | cut -d' ' -f2) in
         [gG]B*) UNIT=1073741824;;
         [mM]B*) UNIT=1048576;;
         [kK]B*) UNIT=1024;;
         B*) UNIT=1;;
    esac
    MEMORY=$(( $(echo "!{task.memory}" | cut -d '.' -f1 | cut -d ' ' -f1) * $UNIT ))
    mkdir -p out
    set -x
    java -Xms$((MEMORY/2)) -Xmx$MEMORY -jar /app/trimmomatic/trimmomatic.jar \
      PE -threads !{task.cpus} -phred33 in/readsR1.fastq.gz in/readsR2.fastq.gz \
      out/trimmedR1.fastq.gz /dev/null out/trimmedR2.fastq.gz /dev/null !{params.settings}
    '''
}