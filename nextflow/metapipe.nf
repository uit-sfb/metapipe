// import modules
include {Assembly} from './assembly/assembly.nf'
include {Binning} from './binning/binning.nf'
include {TaxonomicClassification} from './taxonomic-classification/taxonomicClassification.nf'
include {FunctionalAssignment} from './functional-assignment/functionalAssignment.nf'

workflow Metapipe {
  if (params.reads) {
    read_pairs_ch = Channel.fromPath(params.reads, checkIfExists: true).take(2).toSortedList() | view { array ->
    if (array.size == 2)
      "Illumina read files:\n  Forward: ${array[0]}\n  Reverse: ${array[0]}"
    else
      "Illumina read file (interleaved): ${array[0]}"
    }
    Assembly(read_pairs_ch)
    if (!params.noTaxo) {
      TaxonomicClassification(Assembly.out.trimmedR1, Assembly.out.trimmedR2, Assembly.out.pred16s)
      mockDependency = TaxonomicClassification.out.mockDependency
    } else {
      mockDependency = Channel.empty()
    }
    if (!params.noBinning) {
      Binning(Assembly.out.contigs, Assembly.out.trimmedR1, Assembly.out.trimmedR2)
      mockDependency2 = Binning.out.mockDependency
    } else {
      mockDependency2 = Channel.empty()
    }
    if (!params.noFunc)
      FunctionalAssignment(Assembly.out.contigs, mockDependency.mix(mockDependency2) | collect)
  } else {
    contigs = Channel.fromPath(params.contigs, checkIfExists: true).take(1) | view { inp -> "Contig file: $inp"}
    FunctionalAssignment(contigs, Channel.empty())
  }
}
