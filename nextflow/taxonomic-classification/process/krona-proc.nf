process KronaProc {
  label 'taxonomic_classification'

  container 'biocontainers/krona:v2.7.1_cv1'

  input:
    path input, stageAs: 'in/*'

  output:
    path 'out/taxonomy.html', emit: data

  shell:
    '''
    set +u
    mkdir -p out
    set -x
    ARG=""
    for f in in/*; do
      ARG="${ARG} $f,$(basename $f .krona | sed 's/.*/\\u&/')"
    done
    if [[ ! -z "$ARG" ]]; then
      #Do NOT add double quotes to the line below
      ktImportText -o out/taxonomy.html $ARG
    fi
    '''
}