params.refdbDir = "${launchDir}"

process Mapseq2krona {
  label 'taxonomic_classification'

  container "registry.gitlab.com/uit-sfb/metapipe/mapseq2kaiju:${workflow.manifest.version ?: workflow.revision ?: 'master'}"

  input:
    path input, stageAs: 'in/*'

  output:
    path 'out/mapseq.krona', emit: krona

  shell:
    '''
    set +u
    case $(echo "!{task.memory}" | cut -d' ' -f2) in
             [gG]B*) UNIT=1073741824;;
             [mM]B*) UNIT=1048576;;
             [kK]B*) UNIT=1024;;
             B*) UNIT=1;;
        esac
        MEMORY=$(( $(echo "!{task.memory}" | cut -d '.' -f1 | cut -d ' ' -f1) * $UNIT ))
    mkdir -p out
    set -x
    /opt/docker/bin/mapseq2kaiju -J-Xms$((MEMORY/2)) -J-Xmx$MEMORY -XX:ActiveProcessorCount=!{task.cpus} -- \
      -i "!{input}" -o out/mapseq.krona
    '''
}