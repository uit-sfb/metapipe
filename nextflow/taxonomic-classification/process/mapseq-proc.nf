params.refdbDir = "${launchDir}"

process MapseqProc {
  label 'taxonomic_classification'

  container 'registry.gitlab.com/uit-sfb/genomic-tools/mapseq:1.2.6'

  input:
    path input, stageAs: 'in/*'

  output:
    path 'out/mapseq.out', emit: data
    path 'out/mapseq.tsv', emit: tsv

  shell:
    '''
    set +u
    mkdir -p out
    set -x
    /app/mapseq/mapseq -nthreads !{task.cpus} !{input} > out/mapseq.out
    /app/mapseq/mapseq -otucounts out/mapseq.out > out/mapseq.tsv
    '''
}