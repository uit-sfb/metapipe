params.refdbDir = "${launchDir}"

process KaijuProc {
  label 'taxonomic_classification'

  container 'registry.gitlab.com/uit-sfb/genomic-tools/kaiju:1.8.2'
  containerOptions = { if (workflow.containerEngine == "docker") "-v ${params.refdbDir}:/refdb" else if (workflow.containerEngine == "singularity") "-B ${params.refdbDir}:/refdb" else "" }
  memory { "$dbsize".toLong().B + 4.GB }

  input:
    val refdb
    path inputR1, stageAs: 'in/*'
    path inputR2, stageAs: 'in/*'
    val dbsize

  output:
    path 'out/kaiju.tsv', emit: data
    path 'out/kaiju.krona', emit: krona

  shell:
    '''
    set +u
    mkdir -p out
    DB_PATH="!{refdb}/db/kaiju/$(ls "!{refdb}/db/kaiju")"
    set -x
    /app/kaiju/kaiju -t $DB_PATH/nodes.dmp -f $DB_PATH/*.fmi \
      -i !{inputR1} -j !{inputR2} -o out/kaiju.out -z !{task.cpus}
    /app/kaiju/kaiju2table -t $DB_PATH/nodes.dmp -n $DB_PATH/names.dmp -r species -o out/kaiju.tsv out/kaiju.out
    /app/kaiju/kaiju2krona -t $DB_PATH/nodes.dmp -n $DB_PATH/names.dmp -i out/kaiju.out -o out/kaiju.krona
    '''
}