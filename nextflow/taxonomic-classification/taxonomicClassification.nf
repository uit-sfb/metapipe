include {Kaiju} from './kaiju.nf' params(refdb: params.kaiju_refdb, refdbDir: params.refdbDir)
include {Mapseq} from './mapseq.nf' params(refdb: params.mapseq_refdb, refdbDir: params.refdbDir)
include {KronaProc} from './process/krona-proc.nf'
include {Export} from '../helper/export.nf'

workflow TaxonomicClassification {
  take:
    trimmedR1
    trimmedR2
    pred16s

  main:
    Kaiju(trimmedR1, trimmedR2)
    Mapseq(pred16s)
    krona_ch = Kaiju.out.krona.mix(Mapseq.out.krona) | collect
    KronaProc(krona_ch)
    export_ch = Kaiju.out.data.mix(Mapseq.out.data, KronaProc.out.data) | collect
    Export("taxonomicClassification", export_ch, false)

  emit:
    mockDependency = Kaiju.out.done
}