params.refdbDir = "${launchDir}"
params.refdb = 'default'

include {MapseqProc} from './process/mapseq-proc.nf'
include {Mapseq2krona} from './process/mapseq2krona.nf'

workflow Mapseq {
  take:
    input

  main:
  if (params.refdb != "") {
    MapseqProc(input)
    data_ch = MapseqProc.out.data
    Mapseq2krona(MapseqProc.out.tsv)
    krona_ch = Mapseq2krona.out.krona
  } else {
    data_ch = Channel.empty()
    krona_ch = Channel.empty()
  }

  emit:
    data = data_ch
    krona = krona_ch
}
