params.refdbDir = "${launchDir}"
params.refdb = 'kaiju-mar:1.6-1.5'

include {DownloadRefDb} from '../helper/downloadRefDb.nf' params(refdbDir: params.refdbDir)
include {KaijuProc} from './process/kaiju-proc.nf'

def mem(refdb) {
  file("${refdb}/db/kaiju/**/*.fmi")[0].size()
}

workflow Kaiju {
  take:
    inputR1
    inputR2

  main:
    if (params.refdb != "") {
      DownloadRefDb(params.refdb)
      dbsize = DownloadRefDb.out.extDbPath.map{path -> mem(path)}
      KaijuProc(DownloadRefDb.out.dbPath, inputR1, inputR2, dbsize)
      data_ch = KaijuProc.out.data
      krona_ch = KaijuProc.out.krona
    } else {
      data_ch = Channel.empty()
      krona_ch = Channel.empty()
    }

  emit:
    data = data_ch
    krona = krona_ch
    done = true
}
