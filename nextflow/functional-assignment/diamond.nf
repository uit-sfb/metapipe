params.refdbDir = "${launchDir}"
params.refdb = 'diamond-marref-proteins:1.6'

include {DownloadRefDb} from '../helper/downloadRefDb.nf' params(refdbDir: params.refdbDir)
include {DiamondProc} from './process/diamond-proc.nf'

workflow Diamond {
  take:
    input
    mockDependency

  main:
    if (params.refdb != "") {
      DownloadRefDb(params.refdb)
      refdb = DownloadRefDb.out.dbPath
      DiamondProc(refdb, input, mockDependency)
      out_ch = DiamondProc.out.data
    } else {
      out_ch = Channel.empty()
    }

  emit:
    data = out_ch
}