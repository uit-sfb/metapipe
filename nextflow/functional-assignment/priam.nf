params.refdbDir = "${launchDir}"
params.refdb = 'priam:JAN18'

include {DownloadRefDb} from '../helper/downloadRefDb.nf' params(refdbDir: params.refdbDir)
include {PriamSearchProc} from './process/priamSearch-proc.nf'

workflow Priam {
  take:
    input
    mockDependency

  main:
    if (params.refdb != "") {
      DownloadRefDb(params.refdb)
      refdb = DownloadRefDb.out.dbPath
      PriamSearchProc(refdb, input, mockDependency)
      genomeECs = PriamSearchProc.out.genomeECs | collectFile()
      genomeEnzymes = PriamSearchProc.out.genomeEnzymes | collectFile()
      predictableECs = PriamSearchProc.out.predictableECs | collectFile()
      sequenceECs = PriamSearchProc.out.sequenceECs | collectFile()
      out_ch = PriamSearchProc.out.genomeEnzymes
      out_collected_ch = genomeECs.mix(genomeEnzymes, predictableECs, sequenceECs) | collect
    } else {
      out_ch = Channel.empty()
      out_collected_ch = Channel.empty()
    }

  emit:
    enzymes = out_ch
    data = out_collected_ch
}
