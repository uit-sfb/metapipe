params.refdbDir = "${launchDir}"
params.refdb = 'interpro:5.55-88.0'
params.interpro_apps = 'TIGRFAM,SMART,ProSiteProfiles,ProSitePatterns,HAMAP,SUPERFAMILY,PRINTS,GENE3D,PIRSF,COILS'

include {DownloadRefDb} from '../helper/downloadRefDb.nf' params(refdbDir: params.refdbDir)
include {InterproscanProc} from './process/interproscan-proc.nf'

workflow Interproscan {
  take:
    input
    mockDependency

  main:
    if (params.refdb != "") {
      DownloadRefDb(params.refdb)
      refdb = DownloadRefDb.out.dbPath
      out_ch = InterproscanProc(refdb, input, mockDependency)
    } else {
      out_ch = Channel.empty()
    }

  emit:
    data = out_ch
}