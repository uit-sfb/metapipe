include {PreProcessContigs} from './process/preProcessContigs.nf' params(contigsCutoff: params.contigsCutoff)
include {SplitCds} from './process/splitCds.nf'
include {Mga} from './process/mga.nf'
include {Gff} from './process/gff.nf'
include {GeneExtractor} from './process/geneExtractor.nf' params(removeIncompleteGenes: params.removeIncompleteGenes)
include {Priam} from './priam.nf' params(refdb: params.priam_refdb, refdbDir: params.refdbDir)
include {Diamond} from './diamond.nf' params(refdb: params.diamond_refdb, sensitivity: params.diamond_sensitivity, refdbDir: params.refdbDir)
include {Interproscan} from './interproscan.nf' params(refdb: params.interpro_refdb, refdbDir: params.refdbDir, interpro_apps: params.interpro_apps)
include {MergeGff} from './process/mergeGff.nf'
include {Export} from '../helper/export.nf'

workflow FunctionalAssignment {
  take:
    contigs
    mockDependency

  main:
    contigs_split = PreProcessContigs(contigs) | flatten | map { path -> tuple(path.baseName, "${path}/contigs.fasta") }
    mga = Mga(contigs_split) | flatten | map { path -> tuple(path.parent.baseName, path) }
    contigs_split.join(mga) | GeneExtractor
    cds_split = GeneExtractor.out.cds_prot | flatten | map { path -> tuple(path.parent.baseName, path) }
    cds_nuc = GeneExtractor.out.cds_nuc | collectFile()
    cds_prot = GeneExtractor.out.cds_prot | collectFile()
    Priam(cds_split, mockDependency.last())
    priam = Priam.out.enzymes | flatten | map { path -> tuple(path.parent.baseName, path) }
    diamond = Diamond(cds_split, mockDependency.last()) | flatten | map { path -> tuple(path.parent.baseName, path) }
    interpro = Interproscan(cds_split, mockDependency.last()) | flatten | map { path -> tuple(path.parent.baseName, path) }
    //In case some of the tools are disabled, remainder = true will create null paths, which breaks gff
    //So we filter null out and we discard the head element (DATUM) since we don't need it anymore
    //That way we get a pure list of files which is more flexible to handle.
    files = mga.join(priam, remainder: true).join(diamond, remainder: true).join(interpro, remainder: true) | map { it.findAll{ it != null } } | map { it[1..-1] }
    partGff = files | Gff | collectFile(keepHeader: true, skip: 1, newLine: true)
    gff = MergeGff(partGff, cds_prot)
    export_ch = cds_prot.mix(cds_nuc, Diamond.out.data | collectFile(), Interproscan.out.data | collectFile(), gff, Priam.out.data) | collect
    Export("functionalAssignment", export_ch, false)
}