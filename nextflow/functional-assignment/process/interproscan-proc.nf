params.refdbDir = "${launchDir}"
params.interpro_apps = "TIGRFAM,SMART,ProSiteProfiles,ProSitePatterns,HAMAP,SUPERFAMILY,PRINTS,GENE3D,PIRSF,COILS"

//Issue with Singularity:
//Runs from /app/interpro, where interproscan.properties and data/ are expected to be.
//Problem: /app/interpro is read-only

process InterproscanProc {
  label 'functional_assignment'
  tag "$DATUM"

  //For compatibility reasons, the docker image used should be matching the data version
  //substringAfter(":") doesn't work as for some reason only substring() is available
  container "registry.gitlab.com/uit-sfb/genomic-tools/interproscan:${params.refdb.substring(9)}"
  containerOptions = { if (workflow.containerEngine == "docker") "-v ${params.refdbDir}:/refdb" else if (workflow.containerEngine == "singularity") "-B ${params.refdbDir}:/refdb" else "" }

  ext.toolsCpu = 1
  ext.precalcService = ''

  input:
    val refdb
    tuple val(DATUM), path(input, stageAs: 'in/*')
    val flag

  output:
    path "out/slices/${DATUM}/interpro.out", emit: data

  shell:
    '''
    set +u
    #The memory setting is not used since most of the memory usage comes from processes spawned outside of the JVM
    MAX_WORKERS=$(( (!{task.cpus} - 1) / !{task.ext.toolsCpu} ))
    if [[ $MAX_WORKERS < 1  ]]; then MAX_WORKERS=1; fi
    #Interproscan expects the data at this location (./data)
    ln -s "!{refdb}/db/interpro" /app/interpro/data
    OUT_DIR="out/slices/!{DATUM}"
    mkdir -p "$OUT_DIR"
    if [[ "!{task.ext.precalcService}" != "default" ]]; then
       if [[ -z "!{task.ext.precalcService}" ]]; then
         echo "Disabled precalc service"
       else
         echo "Using custom service !{task.ext.precalcService}"
       fi
       sed -r -i 's<(precalculated.match.lookup.service.url=).*<\\1!{task.ext.precalcService}<' /app/interpro/interproscan.properties
    else
       echo "Using EBI precalc service"
    fi
    sed -r -i 's<(number\\.of\\.embedded\\.workers=)([0-9]*)<\\1'$MAX_WORKERS'<' /app/interpro/interproscan.properties
    sed -r -i 's<(maxnumber\\.of\\.embedded\\.workers=)([0-9]*)<\\1'$MAX_WORKERS'<' /app/interpro/interproscan.properties
    #cat /app/interpro/interproscan.properties
    set -x
    /app/interpro/interproscan.sh -goterms -iprlookup -f tsv --applications !{params.interpro_apps} \
      -i "!{input}" -o "$OUT_DIR/interpro.out" --tempdir tmp/temp
    '''
}