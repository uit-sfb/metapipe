params.refdbDir = "${launchDir}"

process PriamSearchProc {
  label 'functional_assignment'
  tag "$DATUM"

  container 'registry.gitlab.com/uit-sfb/genomic-tools/priamsearch:2.0'
  containerOptions = { if (workflow.containerEngine == "docker") "-v ${params.refdbDir}:/refdb" else if (workflow.containerEngine == "singularity") "-B ${params.refdbDir}:/refdb" else "" }
  input:
    val refdb
    tuple val(DATUM), path(input, stageAs: 'in/*')
    val flag

  output:
    path "out/slices/${DATUM}/genomeECs.txt", emit: genomeECs
    path "out/slices/${DATUM}/genomeEnzymes.txt", emit: genomeEnzymes
    path "out/slices/${DATUM}/predictableECs.txt", emit: predictableECs
    path "out/slices/${DATUM}/sequenceECs.txt", emit: sequenceECs
    val true, emit: done

  shell:
    '''
    set +u
    case $(echo "!{task.memory}" | cut -d' ' -f2) in
         [gG]B*) UNIT=1073741824;;
         [mM]B*) UNIT=1048576;;
         [kK]B*) UNIT=1024;;
         B*) UNIT=1;;
    esac
    MEMORY=$(( $(echo "!{task.memory}" | cut -d '.' -f1 | cut -d ' ' -f1) * $UNIT ))
    DB_PATH="!{refdb}/db/priam/"
    set -x
    java -Xms$((MEMORY/2)) -Xmx$MEMORY -XX:ActiveProcessorCount=!{task.cpus} -jar /app/priamsearch/PRIAM_search.jar -p "$DB_PATH" -np 1 -pt 0.5 -mp 70 -cc T -cg F -n mp -i "!{input}" --out tmp
    set +x
    # Note, we used to split each file by section to be able to merge the slices together by merging by sections
    # But it did not work as '#' may be used in the files in other places than at a section start
    # and led to an overly complicated pipeline.
    mkdir -p out/slices/!{DATUM}
    mv tmp/PRIAM_mp/ANNOTATION/sequenceECs.txt out/slices/!{DATUM}
    mv tmp/PRIAM_mp/ANNOTATION/genomeECs.txt out/slices/!{DATUM}
    mv tmp/PRIAM_mp/ANNOTATION/genomeEnzymes.txt out/slices/!{DATUM}
    mv tmp/PRIAM_mp/ANNOTATION/predictableECs.txt out/slices/!{DATUM}
    '''
}