params.removeIncompleteGenes = false

process GeneExtractor {
  label 'functional_assignment'
  tag "$DATUM"

  container "registry.gitlab.com/uit-sfb/metapipe/gene-extractor:${workflow.manifest.version ?: workflow.revision ?: 'master'}"

  input:
    tuple val(DATUM), path(contigs, stageAs: 'in/contigs/*'), path(mga, stageAs: 'in/mga/*')

  output:
    path "out/slices/${DATUM}/cds.nuc.fasta", emit: cds_nuc
    path "out/slices/${DATUM}/cds.prot.fasta", emit: cds_prot

  shell:
    '''
    set +u
    case $(echo "!{task.memory}" | cut -d' ' -f2) in
         [gG]B*) UNIT=1073741824;;
         [mM]B*) UNIT=1048576;;
         [kK]B*) UNIT=1024;;
         B*) UNIT=1;;
    esac
    MEMORY=$(( $(echo "!{task.memory}" | cut -d '.' -f1 | cut -d ' ' -f1) * $UNIT ))
    if !{params.removeIncompleteGenes}; then
      RMV_NON_COMPLETE_FLAG="--removeNonCompleteGenes";
    else
      RMV_NON_COMPLETE_FLAG="";
    fi
    set -x
    /opt/docker/bin/gene-extractor -J-Xms$((MEMORY/2)) -J-Xmx$MEMORY -XX:ActiveProcessorCount=!{task.cpus} -- --contigsPath "!{contigs}" --mgaOutPath "!{mga}" --outPath "out/slices/!{DATUM}" $RMV_NON_COMPLETE_FLAG
    '''
}