process Gff {
  label 'functional_assignment'

  container "registry.gitlab.com/uit-sfb/metapipe/gff:${workflow.manifest.version ?: workflow.revision ?: 'master'}"

  input:
    path(inputs, stageAs: 'in/*')

  output:
    path "out/features.gff", emit: gff

  shell:
    '''
    set +u
    case $(echo "!{task.memory}" | cut -d' ' -f2) in
         [gG]B*) UNIT=1073741824;;
         [mM]B*) UNIT=1048576;;
         [kK]B*) UNIT=1024;;
         B*) UNIT=1;;
    esac
    MEMORY=$(( $(echo "!{task.memory}" | cut -d '.' -f1 | cut -d ' ' -f1) * $UNIT ))
    OUT_DIR="out"
    ls "in/"
    set -x
    DIAMOND=$(find -L in -type f -name "diamond-*" | tr '\n' ',' | sed 's/,$//')
    INTERPRO=$(find -L in -type f -name "interpro.out" | tr '\n' ',' | sed 's/,$//')
    PRIAM=$(find -L in -type f -name "genomeEnzymes.txt" | tr '\n' ',' | sed 's/,$//')
    PARAMS="--mga in/mga.out ${DIAMOND:+--diamond $DIAMOND}  ${INTERPRO:+--interpro $INTERPRO}  ${PRIAM:+--priam $PRIAM}"
    #Do NOT use double quotes around $PARAMS below
    /opt/docker/bin/gff -J-Xms$((MEMORY/2)) -J-Xmx$MEMORY -XX:ActiveProcessorCount=!{task.cpus} -- \
      --outdir "$OUT_DIR" $PARAMS
    '''
}
