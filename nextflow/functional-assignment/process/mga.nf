process Mga {
  label 'functional_assignment'
  tag "$DATUM"

  container 'registry.gitlab.com/uit-sfb/genomic-tools/mga:1'

  input:
    tuple val(DATUM), path(input, stageAs: 'in/*')

  output:
    path "out/slices/${DATUM}/mga.out", emit: mga

  shell:
    '''
    set +u
    OUT_DIR="out/slices/!{DATUM}"
    mkdir -p "$OUT_DIR" #requires output dir to exist
    if [[ -s !{input} ]]; then
      set -x
      /app/mga/mga_linux_ia64 "!{input}" > "$OUT_DIR/mga.out"
    else
      #Deal with empty input here as mga throws Segmentation fault otherwise
      echo "Empty input"
    fi
    '''
}