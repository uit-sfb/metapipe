process MergeGff {
  label 'functional_assignment'

  input:
    path(gffPart, stageAs: 'in/*')
    path(fasta, stageAs: 'in/*')

  output:
    path "out/features.gff", emit: gff

  shell:
    '''
    set +u
    mkdir -p out
    cp "!{gffPart}" "out/features.gff"
    echo "\n\n##FASTA\n\n" >> "out/features.gff"
    cat "!{fasta}" >> "out/features.gff"
    '''
}
