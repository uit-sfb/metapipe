## Pipeline Schema

The [pipeline schema](https://help.tower.nf/pipeline-schema/overview/) needs to be updated each time the param list is modified.

To do so:
- install [nf-core](https://nf-co.re/tools/#python-package-index)
- run `nf-core schema build`
- customize using the graphical interface if needed